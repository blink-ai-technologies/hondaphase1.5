# Honda Phase 1.5

Include 3 versions of BlinkAI pipeline developed in Honda phase 1.5.

## Usage

Run make command to make the project.

```bash
make
```

Use command ./real_time_demo + pipeline mode + exposure mode to start the pipeline.
Example:

```bash
./real_time_demo blink auto
```

This command will start the pipeline with blink mode and running in auto-exposure.

## Version 1.0 

Process on 1920x1080 resolution. Highest computation. Longest latency. Big FoV.

## Version 2.0

Process on 640x480 resolution. Lowest computation. Shortest latency. Small FoV.

## Version 3.0

Optimized version 1. Medium computation. Medium latency. Big FoV.

## Documentation

Documentation file can be accessed in /Doxygen/html/classBlinkAI.html 

## GPU Frequency

Xavier is running in mode MAXN. Max GPU freq is 1377 MHz. Min GPU freq is 318 MHz.
