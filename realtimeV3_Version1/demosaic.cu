#include <stdio.h>
#include "/usr/local/cuda-10.0/targets/aarch64-linux/include/cuda_fp16.h"
#include "/home/blink/realtimeV3/blink_config.h"
#include <cmath>



__forceinline__ __device__ float full_img(float *output_img,int block_offset,int thread_offset,int total_offset){
        int real_blockId = blockIdx.x + block_offset;
        int real_threadId = threadIdx.x + thread_offset;
        if(real_blockId<0 || real_blockId>527 || real_threadId<0 || real_threadId>959) return float(0.0);
        float out_value = output_img[real_blockId*960+real_threadId+total_offset*528*960];
		if(out_value>1.0) return float(1.0);
        return out_value;
}
__global__ void dequantize(float *input_img,float *output_img, float *de_output_img){
		int nidx = blockIdx.x*blockDim.x+threadIdx.x;
		int idx0 = nidx;
		int idx1 = nidx+528*960;
		int idx2 = nidx+528*960*2;
		int idx3 = nidx+528*960*3;
		
        float x0 = 0.95*output_img[idx0]+0.05*input_img[idx0];
        float x0_2 = x0 * x0;
        de_output_img[idx0] = min((x0_2 + sqrt(x0_2*x0_2 + 16.0/1023.0*x0_2)) / 2.0 + min_pixel, 1.0); // min ~ 1

        
        float x1 = 0.95*output_img[idx1]+0.05*input_img[idx1];
        float x1_2 = x1 * x1;
        de_output_img[idx1] = min((x1_2 + sqrt(x1_2*x1_2 + 16.0/1023.0*x1_2)) / 2.0 + min_pixel, 1.0);
        
        
        float x2 = 0.95*output_img[idx2]+0.05*input_img[idx2];
        float x2_2 = x2 * x2;
        de_output_img[idx2] = min((x2_2 + sqrt(x2_2*x2_2 + 16.0/1023.0*x2_2)) / 2.0 + min_pixel, 1.0);
        
        
        float x3 = 0.95*output_img[idx3]+0.05*input_img[idx3];
        float x3_2 = x3 * x3;
        de_output_img[idx3] = min((x3_2 + sqrt(x3_2*x3_2 + 16.0/1023.0*x3_2)) / 2.0 + min_pixel, 1.0);
        
}
__global__ void demosaic(float *de_output_img, float *img_bgr){
        
        // G R
        // B G
        int idx1 = blockIdx.x*1920*2 + 2*threadIdx.x;
        int idx2 = blockIdx.x*1920*2 + 2*threadIdx.x + 1;
        int idx3 = idx1 + 1920;
        int idx4 = idx2 + 1920;

		//from Gr
        float rr = (full_img(de_output_img,0,0,1) +full_img(de_output_img,0,-1,1))/2.0;
        float rg = full_img(de_output_img,0,0,0);
        float rb = (full_img(de_output_img,-1,0,2) + full_img(de_output_img,0,0,2))/2.0;
        img_bgr[idx1*3]   =  rb;
        img_bgr[idx1*3+1] =  rg;
        img_bgr[idx1*3+2] =  rr;
        
        // from R
        rr = full_img(de_output_img,0,0,1);
		rg = (full_img(de_output_img,0,0,0) + full_img(de_output_img,0,0,3) + full_img(de_output_img,-1,0,3) + full_img(de_output_img,0,1,0))/4.0;
        rb = (full_img(de_output_img,0,0,2) + full_img(de_output_img,-1,0,2) + full_img(de_output_img,-1,1,2) + full_img(de_output_img,0,1,2))/4.0;
        img_bgr[idx2*3]   =  rb ;
        img_bgr[idx2*3+1] =  rg ;
        img_bgr[idx2*3+2] =  rr ;      
		//from B
        rr = (full_img(de_output_img,0,0,1)+full_img(de_output_img,0,-1,1)+full_img(de_output_img,1,0,1)+full_img(de_output_img,1,-1,1))/4.0;
        rg = (full_img(de_output_img,0,0,0)+full_img(de_output_img,0,0,3)+full_img(de_output_img,0,-1,3)+full_img(de_output_img,1,0,0))/4.0;
        rb = full_img(de_output_img,0,0,2);
        img_bgr[idx3*3]   =  rb;
        img_bgr[idx3*3+1] =  rg;
        img_bgr[idx3*3+2] =  rr;      
        //from Gb
        rr = (full_img(de_output_img,0,0,1) +full_img(de_output_img,1,0,1))/2.0;
        rg = full_img(de_output_img,0,0,3);
        rb = (full_img(de_output_img,0,0,2) + full_img(de_output_img,0,1,2))/2.0;
        img_bgr[idx4*3]   =  rb;
        img_bgr[idx4*3+1] =  rg;
        img_bgr[idx4*3+2] =  rr;   
}

__global__ void gray_log(float *img_bgr, float *img_log){
        int idx1 = blockIdx.x*1920*2 + 2*threadIdx.x;
        int idx2 = blockIdx.x*1920*2 + 2*threadIdx.x + 1;
        int idx3 = idx1 + 1920;
        int idx4 = idx2 + 1920;
		float r,g,b;
        b = img_bgr[idx1*3]  ;
        g = img_bgr[idx1*3+1];
        r = img_bgr[idx1*3+2];
        img_log[idx1] = log(min(max(0.144*b + 0.587*g + 0.299*r,min_pixel),1.0));
      
        b = img_bgr[idx2*3]  ;
        g = img_bgr[idx2*3+1];
        r = img_bgr[idx2*3+2];
        img_log[idx2] = log(min(max(0.144*b + 0.587*g + 0.299*r,min_pixel),1.0));       

		
        b = img_bgr[idx3*3]  ;
        g = img_bgr[idx3*3+1];
        r = img_bgr[idx3*3+2];
        img_log[idx3] = log(min(max(0.144*b + 0.587*g + 0.299*r,min_pixel),1.0));        
        
        b = img_bgr[idx4*3]  ;
        g = img_bgr[idx4*3+1];
        r = img_bgr[idx4*3+2];
        img_log[idx4] = log(min(max(0.144*b + 0.587*g + 0.299*r,min_pixel),1.0));    
}



extern "C"  void demosaicWrapper(float *input_img,float *output_img, float *de_output_img, float *img_bgr,float *img_log){
	dequantize<<<528,960>>>(input_img,output_img,de_output_img);
	demosaic<<<530,962>>>(de_output_img, img_bgr);
	gray_log<<<528,960>>>(img_bgr,img_log);
}
