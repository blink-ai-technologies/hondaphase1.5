#include <stdio.h>
#include "/usr/local/cuda-10.0/targets/aarch64-linux/include/cuda_fp16.h"
#include "/home/blink/realtimeV3/blink_config.h"
#include <cmath>
#include <math.h> 

    
__global__ void tone_curve(unsigned char *img_bgr8, float *img_bgr, float *img_log, float *img_base,float *sk,float *vk){ // Map base image according to tone curve
        int row = blockIdx.x;
		int col = threadIdx.x;
		int idx0 = row*1920*2 + 2*col;
		int idx1 = row*1920*2 + 2*col + 1;
		int idx2 = idx0 + 1920;
		int idx3 = idx1 + 1920;
		float sharp = 5.0;
				
		float base0 = img_base[idx0];
		float log0  = img_log[idx0];
		float relative_luma0 = base0 - MINV;
		int bin_index0 = int(relative_luma0/deltaHist);
		float tonedLogLuma0 = (relative_luma0 - bin_index0*deltaHist)*sk[bin_index0] + vk[bin_index0];
		float gain0 = exp((float)(tonedLogLuma0 + (sharp-1.0)*log0 - sharp*base0));
		gain0 = min(max(gain0,1.0),48.0);
		float b0 = img_bgr[idx0*3] ;
		float g0 = img_bgr[idx0*3+1];
		float r0 = img_bgr[idx0*3+2];
		b0 = min(b0 * gain0,1.0);
		g0 = min(g0 * gain0,1.0);
		r0 = min(r0 * gain0,1.0);
		img_bgr8[idx0*3]   = (unsigned char)(b0*255);
		img_bgr8[idx0*3+1] = (unsigned char)(g0*255);
		img_bgr8[idx0*3+2] = (unsigned char)(r0*255);
		//img_bgr[idx0*3]   = b0;
		//img_bgr[idx0*3+1] = g0;
		//img_bgr[idx0*3+2] = r0;
		
		float base1 = img_base[idx1];
		float log1  = img_log[idx1];
		float relative_luma1 = base1 - MINV;
		int bin_index1 = int(relative_luma1/deltaHist);
		float tonedLogLuma1 = (relative_luma1 - bin_index1*deltaHist)*sk[bin_index1] + vk[bin_index1];
		float gain1 = exp((float)(tonedLogLuma1 + (sharp-1.0)*log1 - sharp*base1));
		gain1 = min(max(gain1,1.0),48.0);
		float b1 = img_bgr[idx1*3] ;
		float g1 = img_bgr[idx1*3+1];
		float r1 = img_bgr[idx1*3+2];
		b1 = min(b1 * gain1,1.0);
		g1 = min(g1 * gain1,1.0);
		r1 = min(r1 * gain1,1.0);
		img_bgr8[idx1*3]   = (unsigned char)(b1*255);
		img_bgr8[idx1*3+1] = (unsigned char)(g1*255);
		img_bgr8[idx1*3+2] = (unsigned char)(r1*255);
		//img_bgr[idx1*3]   = b1;
		//img_bgr[idx1*3+1] = g1;
		//img_bgr[idx1*3+2] = r1;
		
		float base2 = img_base[idx2];
		float log2  = img_log[idx2];
		float relative_luma2 = base2 - MINV;
		int bin_index2 = int(relative_luma2/deltaHist);
		float tonedLogLuma2 = (relative_luma2 - bin_index2*deltaHist)*sk[bin_index2] + vk[bin_index2];
		float gain2 = exp((float)(tonedLogLuma2 + (sharp-1.0)*log2 - sharp*base2));
		gain2 = min(max(gain2,1.0),48.0);
		float b2 = img_bgr[idx2*3] ;
		float g2 = img_bgr[idx2*3+1];
		float r2 = img_bgr[idx2*3+2];
		b2 = min(b2 * gain2,1.0);
		g2 = min(g2 * gain2,1.0);
		r2 = min(r2 * gain2,1.0);
		img_bgr8[idx2*3]   = (unsigned char)(b2*255);
		img_bgr8[idx2*3+1] = (unsigned char)(g2*255);
		img_bgr8[idx2*3+2] = (unsigned char)(r2*255);
		//img_bgr[idx2*3]   = b2;
		//img_bgr[idx2*3+1] = g2;
		//img_bgr[idx2*3+2] = r2;
		
		float base3 = img_base[idx3];
		float log3  = img_log[idx3];
		float relative_luma3 = base3 - MINV;
		int bin_index3 = int(relative_luma3/deltaHist);
		float tonedLogLuma3 = (relative_luma3 - bin_index3*deltaHist)*sk[bin_index3] + vk[bin_index3];
		float gain3 = exp((float)(tonedLogLuma3 + (sharp-1.0)*log3 - sharp*base3));
		gain3 = min(max(gain3,1.0),48.0);
		float b3 = img_bgr[idx3*3] ;
		float g3 = img_bgr[idx3*3+1];
		float r3 = img_bgr[idx3*3+2];
		b3 = min(b3 * gain3,1.0);
		g3 = min(g3 * gain3,1.0);
		r3 = min(r3 * gain3,1.0);
		img_bgr8[idx3*3]   = (unsigned char)(b3*255);
		img_bgr8[idx3*3+1] = (unsigned char)(g3*255);
		img_bgr8[idx3*3+2] = (unsigned char)(r3*255);
		//img_bgr[idx3*3]   = b3;
		//img_bgr[idx3*3+1] = g3;
		//img_bgr[idx3*3+2] = r3;		
}

extern "C"  void toneWrapper(unsigned char *img_bgr8, float *img_bgr, float *img_log, float *img_base,float *sk0,float *vk0){
	tone_curve<<<528,960>>>(img_bgr8,img_bgr,img_log,img_base,sk0,vk0);
}
