#include <stdio.h>
#include "/usr/local/cuda-10.0/targets/aarch64-linux/include/cuda_fp16.h"
#include "/home/blink/realtimeV3/blink_config.h"

__global__ void imgNormalize(ushort *d_in, float *d_out){
	//float gain = 2;
        int idx1 = blockIdx.x*3840+2*threadIdx.x+0;
        int idx2 = blockIdx.x*3840+2*threadIdx.x+1;
        int idx3 = blockIdx.x*3840+2*threadIdx.x+1920;
        int idx4 = blockIdx.x*3840+2*threadIdx.x+1921;
        int nidx = blockIdx.x*blockDim.x+threadIdx.x;
        //float min_bound = sqrt(4092.0);
        //d_out[nidx]        = min(max(float(d_in[idx1]>>2)-235,0.0)/128.0,1.0);
        //d_out[nidx+230400] = min(max(float(d_in[idx2]>>2)-235,0.0)/128.0,1.0);
        //d_out[nidx+460800] = min(max(float(d_in[idx3]>>2)-235,0.0)/128.0,1.0);
        //d_out[nidx+691200] = min(max(float(d_in[idx4]>>2)-235,0.0)/128.0,1.0);
        
        //float x1 = min(max(float(d_in[idx1]>>6),0.0),1023.0);
        //float x11 = min(max(sqrt((x1+4)*1023),min_bound),1023.0);
        float x1 = float(d_in[idx1]>>6);
        float x11 = min(sqrt((x1+4)*1023),1023.0);
        d_out[nidx+528*960*0] = x1 / x11;
        
        //float x2 = min(max(float(d_in[idx2]>>6),0.0),1023.0);
        //float x22 = min(max(sqrt((x2+4)*1023),min_bound),1023.0);
        float x2 = float(d_in[idx2]>>6);
        float x22 = min(sqrt((x2+4)*1023),1023.0);
        d_out[nidx+528*960*1] = x2 / x22;
        
        //float x3 = min(max(float(d_in[idx3]>>6),0.0),1023.0);
        //float x33 = min(max(sqrt((x3+4)*1023),min_bound),1023.0);
        float x3 = float(d_in[idx3]>>6);
        float x33 = min(sqrt((x3+4)*1023),1023.0);
        d_out[nidx+528*960*2] = x3 / x33;
        
        //float x4 = min(max(float(d_in[idx4]>>6),0.0),1023.0);
        //float x44 = min(max(sqrt((x4+4)*1023),min_bound),1023.0);
        float x4 = float(d_in[idx4]>>6);
        float x44 = min(sqrt((x4+4)*1023),1023.0);
        d_out[nidx+528*960*3] = x4 / x44;
}

extern "C"  void imgNormalizeWrapper(ushort *d_in, float *d_out){
	imgNormalize<<<528,960>>>(d_in,d_out);
}

