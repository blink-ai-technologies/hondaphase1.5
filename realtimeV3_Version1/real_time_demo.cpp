#include "NvCaffeParser.h"
#include "NvInfer.h"
#include "NvInferPlugin.h"
#include "common.h"
#include "cuda_runtime_api.h"
#include "cuda_fp16.h"
#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstring>
#include <ctime>
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <sys/stat.h>
#include <vector>
#include <string>
#include <fcntl.h>
#include <thread>
#include <unistd.h>
#include <queue>
#include <functional>
#include <errno.h>
#include <bits/unique_ptr.h>
#include <chrono>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <linux/videodev2.h>
#include <time.h>
#include <deque>
#include <pthread.h>
#include <semaphore.h>
#include <cstdlib>
#include <iostream>
#include <experimental/filesystem>

//display includes
#include "video_dec_drm.h"
#include "NvDrmRenderer.h"
#include <drm/drm_fourcc.h>
#include <drm_fourcc.h>
#include <malloc.h>
#include <string.h> 
#include <assert.h>
#include <linux/kd.h>
#include <linux/vt.h>
#include "NvUtils.h"
#include "tegra_drm.h"
#ifndef DOWNSTREAM_TEGRA_DRM
#include "tegra_drm_nvdc.h"
#endif
#include "NvApplicationProfiler.h"
#include "nvbuf_utils.h"
#include "blink_config.h"

// real_sense
#include <librealsense2/rs.hpp> // Include RealSense Cross Platform API
#include <opencv2/opencv.hpp>   // Include OpenCV API
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/core/cuda.hpp>
#include <opencv2/cudaarithm.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/cudawarping.hpp>
#include <opencv2/intensity_transform.hpp>
typedef unsigned short ushort;
#define FROM_DISK true       
static const int WIDTH = 1920;
static const int HEIGHT = 1056;
static const int width = 640;
static const int height = 480;
static const int buffer_size = 3;
static const int exposure_time = 333;
unsigned char * output_image;
unsigned char * output_image_default;
unsigned char * bgra_out;
ushort * img_input_buffer;
unsigned char * img_bgr8T;
float * infer_buffer;
float * log_img;
float * cuda_in;
float * cuda_out;
float * Gaussian_img;
float * sk_buffer;
float * hist_buffer;
   

using namespace nvinfer1;
using namespace nvcaffeparser1;
using namespace std;
using namespace plugin;
static bool exit_now = false;
static Logger gLogger;
 

extern "C" void imgNormalizeWrapper(ushort *d_in, float *d_out);
extern "C" void demosaicWrapper(float *input_img,float *output_img, float *de_output_img, float *img_bgr,float *img_log);
extern "C" void toneWrapper(unsigned char *img_bgr8, float *img_bgr, float *img_log,float *img_base,float *sk0,float *vk0);
const char* INPUT_BLOB_IMAGE_NAME   = "denoise_input_image";
const char* INPUT_BLOB_HIDDEN_NAME  = "denoise_input_hidden";
const char* OUTPUT_BLOB_IMAGE_NAME  = "denoise_output_image";
const char* OUTPUT_BLOB_HIDDEN_NAME = "denoise_output_hidden";


void throw_perror(function<void (stringstream & s)> f) {
    stringstream s;
    f(s);
    perror(s.str().c_str());
    throw runtime_error(s.str());
}

// Load weights from files shared with TensorRT samples.
// TensorRT weight files have a simple space delimited format:
// [type] [size] <data x size in hex>
std::map<std::string, Weights> loadWeights(const std::string file)
{
    std::cout << "Loading weights: " << file << std::endl;
    std::map<std::string, Weights> weightMap;

    // Open weights file
    std::ifstream input(file);
    assert(input.is_open() && "Unable to load weight file.");

    // Read number of weight blobs
    int32_t count;
    input >> count;
    assert(count > 0 && "Invalid weight map file.");

    while (count--)
    {
        Weights wt{DataType::kHALF, nullptr, 0};
        //Weights wt{DataType::kFLOAT, nullptr, 0};
        uint32_t type, size;

        // Read name and type of blob
        std::string name;
        input >> name >> std::dec >> type >> size;
        wt.type = static_cast<DataType>(type);

        // Load blob
        if (wt.type == DataType::kFLOAT)
        {
            uint32_t* val = reinterpret_cast<uint32_t*>(malloc(sizeof(val) * size));
            for (uint32_t x = 0, y = size; x < y; ++x)
            {
                input >> std::hex >> val[x];
            }
            wt.values = val;
        }
        else if (wt.type == DataType::kHALF)
        {
            uint16_t* val = reinterpret_cast<uint16_t*>(malloc(sizeof(val) * size));
            for (uint32_t x = 0, y = size; x < y; ++x)
            {
                input >> std::hex >> val[x];
            }
            wt.values = val;
        } 

        wt.count = size;
        weightMap[name] = wt;
    }

    return weightMap;
}

// We have the data files located in a specific directory. This
// searches for that directory format from the current directory.
std::string locateFile(const std::string& input)
{
    std::vector<std::string> dirs{"data/samples/mnist/", "data/mnist/", "data/"};
    return locateFile(input, dirs);
}

class Deconv_IOutputDimensionsFormula:public IOutputDimensionsFormula{
	public:
		DimsHW compute(DimsHW inputDims, DimsHW kernelSize, DimsHW stride, DimsHW padding, DimsHW dilation, const char *layerName) const{
			//int new_h = (inputDims.h()-1)*stride.h() + kernelSize.h() - 2*padding.h() + 1;
            //int new_w = (inputDims.w()-1)*stride.w() + kernelSize.w() - 2*padding.w() + 1;
			int new_h = inputDims.h()*stride.h();
            int new_w = inputDims.w()*stride.w();
			return DimsHW{new_h,new_w};
		}
};
// Creat the engine using only the API and not any parser.
ICudaEngine* createLowLightEngine(unsigned int maxBatchSize, IBuilder* builder, DataType dt)
{

    std::map<std::string, Weights> weightMap = loadWeights(locateFile("Honda_FP32.wts"));
    IOutputDimensionsFormula *deconv_dimensionFormula; 
    Deconv_IOutputDimensionsFormula deconv_outputDimensionsFormula;
    deconv_dimensionFormula = &deconv_outputDimensionsFormula;

    INetworkDefinition* network = builder->createNetwork();

    network->setDeconvolutionOutputDimensionsFormula(deconv_dimensionFormula);

    // Create input tensor of shape { 1, 1, 128, 128 } with name INPUT_BLOB_NAME
    ITensor* data = network->addInput(INPUT_BLOB_IMAGE_NAME, dt, DimsCHW{4, 528, 960});
    ITensor* hidden_data = network->addInput(INPUT_BLOB_HIDDEN_NAME, dt, DimsCHW{12, 264, 480});
    assert(data);
    assert(hidden_data);

    IConvolutionLayer* d1_conv1 = network->addConvolution(*data, 12 , DimsHW{3, 3}, weightMap["d1_conv1.weight"], weightMap["d1_conv1.bias"]);
    assert(d1_conv1);
    d1_conv1->setPadding(DimsHW{1,1});
    d1_conv1->setStride(DimsHW{1, 1});
    d1_conv1->setName("d1_conv1");

    // Add activation layer using the ReLU algorithm.
    IActivationLayer* d1_relu1 = network->addActivation(*d1_conv1->getOutput(0), ActivationType::kRELU);
    assert(d1_relu1);
    d1_relu1->setName("d1_relu1");

    IConvolutionLayer* d1_conv2 = network->addConvolution(*d1_relu1->getOutput(0), 12 , DimsHW{3, 3}, weightMap["d1_conv2.weight"], weightMap["d1_conv2.bias"]);
    assert(d1_conv2);
    d1_conv2->setPadding(DimsHW{1,1});
    d1_conv2->setStride(DimsHW{1, 1});
    d1_conv2->setName("d1_conv2");    

    // Add activation layer using the ReLU algorithm.
    IActivationLayer* d1_relu2 = network->addActivation(*d1_conv2->getOutput(0), ActivationType::kRELU);
    assert(d1_relu2);
    d1_relu2->setName("d1_relu2");


    // Add convolution layer with 16 outputs and a 3x3 filter.
    IConvolutionLayer* d1_conv3 = network->addConvolution(*d1_relu2->getOutput(0), 12 , DimsHW{3, 3}, weightMap["d1_conv3.weight"], weightMap["d1_conv3.bias"]);
    assert(d1_conv3);
    d1_conv3->setPadding(DimsHW{1,1});
    d1_conv3->setStride(DimsHW{1, 1});
    d1_conv3->setName("d1_conv3");

    // Add activation layer using the ReLU algorithm.
    IActivationLayer* d1_relu3 = network->addActivation(*d1_conv3->getOutput(0), ActivationType::kRELU);
    assert(d1_relu3);
    d1_relu3->setName("d1_relu3");

    //hidden_data = d1_relu3->getOutput(0);

    // Add max pooling layer with stride of 2x2 and kernel size of 2x2.
    IPoolingLayer* d1_pool1 = network->addPooling(*d1_relu3->getOutput(0), PoolingType::kAVERAGE, DimsHW{3, 3});
    assert(d1_pool1);
    d1_pool1->setPadding(DimsHW{1,1});
    d1_pool1->setStride(DimsHW{2, 2});
    d1_pool1->setName("d1_pool1");

    // Add convolution layer with 16 outputs and a 3x3 filter.
    IConvolutionLayer* d2_conv1 = network->addConvolution(*d1_pool1->getOutput(0), 12 , DimsHW{3, 3}, weightMap["d2_conv1.weight"], weightMap["d2_conv1.bias"]);
    assert(d2_conv1);
    d2_conv1->setPadding(DimsHW{1,1});
    d2_conv1->setStride(DimsHW{1, 1});
    d2_conv1->setName("d2_conv1");

    // Add activation layer using the ReLU algorithm.
    IActivationLayer* d2_relu1 = network->addActivation(*d2_conv1->getOutput(0), ActivationType::kRELU);
    assert(d2_relu1);
    d2_relu1->setName("d2_relu1");

    // Add convolution layer with 16 outputs and a 3x3 filter.
    IConvolutionLayer* d2_conv2 = network->addConvolution(*d2_relu1->getOutput(0), 12 , DimsHW{3, 3}, weightMap["d2_conv2.weight"], weightMap["d2_conv2.bias"]);
    assert(d2_conv2);
    d2_conv2->setPadding(DimsHW{1,1});
    d2_conv2->setStride(DimsHW{1, 1});
    d2_conv2->setName("d2_conv2");

    // Add activation layer using the ReLU algorithm.
    IActivationLayer* d2_relu2 = network->addActivation(*d2_conv2->getOutput(0), ActivationType::kRELU);
    assert(d2_relu2);
    d2_relu2->setName("d2_relu2");

    ITensor* concat_inputs[] = {hidden_data, d2_relu2->getOutput(0)};
    IConcatenationLayer* concat = network->addConcatenation(concat_inputs, 2);
    assert(concat);
    concat->setName("concat");

    // Add convolution layer with 16 outputs and a 3x3 filter.
    IConvolutionLayer* d2_conv3 = network->addConvolution(*concat->getOutput(0), 12 , DimsHW{5, 5}, weightMap["d2_conv3.weight"], weightMap["d2_conv3.bias"]);
    assert(d2_conv3);
    d2_conv3->setPadding(DimsHW{2,2});
    d2_conv3->setStride(DimsHW{1, 1});
    d2_conv3->setName("d2_conv3");

    // Add activation layer using the ReLU algorithm.
    IActivationLayer* d2_relu3 = network->addActivation(*d2_conv3->getOutput(0), ActivationType::kRELU);
    assert(d2_relu3);
    d2_relu3->setName("d2_relu3");


    IActivationLayer* hidden_output = network->addActivation(*d2_relu3->getOutput(0),ActivationType::kCLIP);
    hidden_output->setAlpha(0.0);
    hidden_output->setBeta(1.0);
    assert(hidden_output);
    hidden_output->setName("hidden_output");


    // Add max pooling layer with stride of 2x2 and kernel size of 2x2.
    IPoolingLayer* d2_pool1 = network->addPooling(*d2_relu3->getOutput(0), PoolingType::kAVERAGE, DimsHW{3, 3});
    assert(d2_pool1);
    d2_pool1->setPadding(DimsHW{1,1});
    d2_pool1->setStride(DimsHW{2, 2});
    d2_pool1->setName("d2_pool1");

    // Add convolution layer with 16 outputs and a 3x3 filter.
    IConvolutionLayer* b_conv1 = network->addConvolution(*d2_pool1->getOutput(0), 24 , DimsHW{3, 3}, weightMap["b_conv1.weight"], weightMap["b_conv1.bias"]);
    assert(b_conv1);
    b_conv1->setPadding(DimsHW{1,1});
    b_conv1->setStride(DimsHW{1, 1});
    b_conv1->setName("b_conv1");

    // Add activation layer using the ReLU algorithm.
    IActivationLayer* b_relu1 = network->addActivation(*b_conv1->getOutput(0), ActivationType::kRELU);
    assert(b_relu1);
    b_relu1->setName("b_relu1");


    // Add convolution layer with 16 outputs and a 3x3 filter.
    IConvolutionLayer* b_conv2 = network->addConvolution(*b_relu1->getOutput(0), 24 , DimsHW{3, 3}, weightMap["b_conv2.weight"], weightMap["b_conv2.bias"]);
    assert(b_conv2);
    b_conv2->setPadding(DimsHW{1,1});
    b_conv2->setStride(DimsHW{1, 1});
    b_conv2->setName("b_conv2");

    // Add activation layer using the ReLU algorithm.
    IActivationLayer* b_relu2 = network->addActivation(*b_conv2->getOutput(0), ActivationType::kRELU);
    assert(b_relu2);
    b_relu2->setName("b_relu2");

    // Add convolution layer with 16 outputs and a 3x3 filter.
    IConvolutionLayer* b_conv3 = network->addConvolution(*b_relu2->getOutput(0), 12 , DimsHW{3, 3}, weightMap["b_conv3.weight"], weightMap["b_conv3.bias"]);
    assert(b_conv3);
    b_conv3->setPadding(DimsHW{1,1});
    b_conv3->setStride(DimsHW{1, 1});
    b_conv3->setName("b_conv3");

    // Add activation layer using the ReLU algorithm.
    IActivationLayer* b_relu3 = network->addActivation(*b_conv3->getOutput(0), ActivationType::kRELU);
    assert(b_relu3);
    b_relu3->setName("b_relu3");


    IDeconvolutionLayer* u1_conv1 = network->addDeconvolution(*b_relu3->getOutput(0), 12 , DimsHW{2, 2}, weightMap["u1_conv1.weight"], weightMap["u1_conv1.bias"]);
    assert(u1_conv1);
    u1_conv1->setPadding(DimsHW{0,0});
    u1_conv1->setStride(DimsHW{2, 2});
    u1_conv1->setName("u1_conv1");

    // Add activation layer using the ReLU algorithm.
    IActivationLayer* u1_relu1 = network->addActivation(*u1_conv1->getOutput(0), ActivationType::kRELU);
    assert(u1_relu1);
    u1_relu1->setName("u1_relu1");

    ITensor* u1_concat_inputs[] = {d2_relu3->getOutput(0), u1_relu1->getOutput(0)};
    IConcatenationLayer* u1_concat = network->addConcatenation(u1_concat_inputs, 2);
    assert(u1_concat);
    u1_concat->setName("u1_concat");
        
    // Add convolution layer with 16 outputs and a 3x3 filter.
    IConvolutionLayer* u1_conv2 = network->addConvolution(*u1_concat->getOutput(0), 12 , DimsHW{3, 3}, weightMap["u1_conv2.weight"], weightMap["u1_conv2.bias"]);
    assert(u1_conv2);
    u1_conv2->setPadding(DimsHW{1,1});
    u1_conv2->setStride(DimsHW{1, 1});
    u1_conv2->setName("u1_conv2");

    // Add activation layer using the ReLU algorithm.
    IActivationLayer* u1_relu2 = network->addActivation(*u1_conv2->getOutput(0), ActivationType::kRELU);
    assert(u1_relu2);
    u1_relu2->setName("u1_relu2");

    // Add convolution layer with 16 outputs and a 3x3 filter.
    IConvolutionLayer* u1_conv3 = network->addConvolution(*u1_relu2->getOutput(0), 12 , DimsHW{3, 3}, weightMap["u1_conv3.weight"], weightMap["u1_conv3.bias"]);
    assert(u1_conv3);
    u1_conv3->setPadding(DimsHW{1,1});
    u1_conv3->setStride(DimsHW{1, 1});
    u1_conv3->setName("u1_conv3");

    // Add activation layer using the ReLU algorithm.
    IActivationLayer* u1_relu3 = network->addActivation(*u1_conv3->getOutput(0), ActivationType::kRELU);
    assert(u1_relu3);
    u1_relu3->setName("u1_relu3");

    IDeconvolutionLayer* u2_conv1 = network->addDeconvolution(*u1_relu3->getOutput(0), 12 , DimsHW{2, 2}, weightMap["u2_conv1.weight"], weightMap["u2_conv1.bias"]);
    assert(u2_conv1);
    u2_conv1->setPadding(DimsHW{0,0});
    u2_conv1->setStride(DimsHW{2, 2});
    u2_conv1->setName("u2_conv1");

    //IDeconvolutionLayer* u2_conv1 = network->addDeconvolution(*u1_relu3->getOutput(0), 16 , DimsHW{2, 2}, weightMap["u2_conv1.weight"], weightMap["u2_conv1.bias"]);
    //assert(u2_conv1);
    //u2_conv1->setPadding(DimsHW{0,0});
    //u2_conv1->setStride(DimsHW{2, 2});
    //u2_conv1->setName("u2_conv1");

    // Add activation layer using the ReLU algorithm.
    IActivationLayer* u2_relu1 = network->addActivation(*u2_conv1->getOutput(0), ActivationType::kRELU);
    assert(u2_relu1);
    u2_relu1->setName("u2_relu1");

    ITensor* u2_concat_inputs[] = {d1_relu3->getOutput(0), u2_relu1->getOutput(0)};
    IConcatenationLayer* u2_concat = network->addConcatenation(u2_concat_inputs, 2);
    assert(u2_concat);
    u2_concat->setName("u2_concat");

    // Add convolution layer with 16 outputs and a 3x3 filter.
    IConvolutionLayer* u2_conv2 = network->addConvolution(*u2_concat->getOutput(0), 12 , DimsHW{3, 3}, weightMap["u2_conv2.weight"], weightMap["u2_conv2.bias"]);
    assert(u2_conv2);
    u2_conv2->setPadding(DimsHW{1,1});
    u2_conv2->setStride(DimsHW{1, 1});
    u2_conv2->setName("u2_conv2");

    // Add activation layer using the ReLU algorithm.
    IActivationLayer* u2_relu2 = network->addActivation(*u2_conv2->getOutput(0), ActivationType::kRELU);
    assert(u2_relu2);
    u2_relu2->setName("u2_relu2");

    // Add convolution layer with 16 outputs and a 3x3 filter.
    IConvolutionLayer* u2_conv3 = network->addConvolution(*u2_relu2->getOutput(0), 16 , DimsHW{3, 3}, weightMap["u2_conv3.weight"], weightMap["u2_conv3.bias"]);
    assert(u2_conv3);
    u2_conv3->setPadding(DimsHW{1,1});
    u2_conv3->setStride(DimsHW{1, 1});
    u2_conv3->setName("u2_conv3");

    // Add activation layer using the ReLU algorithm.
    IActivationLayer* u2_relu3 = network->addActivation(*u2_conv3->getOutput(0), ActivationType::kRELU);
    assert(u2_relu3);
    u2_relu3->setName("u2_relu3");

     // Add convolution layer with 16 outputs and a 3x3 filter.
    IConvolutionLayer* final_conv = network->addConvolution(*u2_relu3->getOutput(0), 4 , DimsHW{3, 3}, weightMap["final.weight"], weightMap["final.bias"]);
    assert(final_conv);
    final_conv->setPadding(DimsHW{1,1});
    final_conv->setStride(DimsHW{1, 1});
    final_conv->setName("final_conv");

    // Add activation layer using the ReLU algorithm.
    IActivationLayer* final_relu = network->addActivation(*final_conv->getOutput(0), ActivationType::kRELU);
    assert(final_relu);
    final_relu->setName("final_relu");

    final_relu->getOutput(0)->setName(OUTPUT_BLOB_IMAGE_NAME);
    network->markOutput(*final_relu->getOutput(0));

    hidden_output->getOutput(0)->setName(OUTPUT_BLOB_HIDDEN_NAME);
    network->markOutput(*hidden_output->getOutput(0));
    
    // Build engine
    builder->setMaxBatchSize(maxBatchSize);
    builder->setMaxWorkspaceSize(16 << 20);
    //builder->setFp16Mode(true);
    ICudaEngine* engine = builder->buildCudaEngine(*network);

    // Don't need the network any more
    network->destroy();

    // Release host memory
    for (auto& mem : weightMap)
    {
        free((void*) (mem.second.values));
    }

    return engine;
}

void APIToModel(unsigned int maxBatchSize, IHostMemory** modelStream)
{
    // Create builder
    IBuilder* builder = createInferBuilder(gLogger);
    builder->setFp16Mode(true);

    // Create model to populate the network, then set the outputs and create an engine
    ICudaEngine* engine = createLowLightEngine(maxBatchSize, builder, DataType::kFLOAT);
    //ICudaEngine* engine = createLowLightEngine_TEST(maxBatchSize, builder, DataType::kFLOAT);
    assert(engine != nullptr);

    // Serialize the engine
    (*modelStream) = engine->serialize();

    std::ofstream ofs("model/lowlightV1.engine", std::ios::out | std::ios::binary);
    ofs.write((char*)((*modelStream)->data()), (*modelStream)->size());
    ofs.close();

    // Close everything down
    engine->destroy();
    builder->destroy();
}

// Returns empty string iff can't read the file
string readBuffer(string const& path)
{
    string buffer;
    ifstream stream(path.c_str(), ios::binary);

    if (stream)
    {
        stream >> noskipws;
        copy(istream_iterator<char>(stream), istream_iterator<char>(), back_inserter(buffer));
    }

    return buffer;
}


using namespace cv;
using namespace std::chrono; 
ushort* input_buffer;
int infer_value;
int tone_value;
int awb_value;
int display_value;
int default_value;

sem_t InferSem;
sem_t ToneSem;
sem_t AWBSem;
sem_t DisplaySem;
sem_t DefaultSem;

void* cuda_buffers[4];
void* cuda_buffer_in;
void* cuda_buffer_out;
void* norm_input;
void* de_img;
void* output_buffer;
void* output_buffer8T;
void* log_buffer;
void* base_buffer;
void* sk_0;
void* vk_0;
float Capture_FPS = 30.0;
float Inference_FPS = 30.0;
float Tone_FPS = 30.0;
float AWB_FPS = 30.0;
float Display_FPS = 30.0;

float pt;
float r = log(255.0);
float rdelta = r/deltaHist;
int histSize[1] = {NBIN};
float hranges[2] = {MINV,0+1E-08F}; // +1E-08F
const float* ranges[1] = {hranges};
float GAMMA = 0.55;
int image_inputIndex;
int hidden_inputIndex;
int image_outputIndex;
int hidden_outputIndex;

context_t ctx_display;
IExecutionContext* context;
static int auto_exposure;
static int Capture_count = 0;
static int Tone_count = 0;
static int AWB_count = 0;
static int Inference_count = 0;
static int Display_count = 0;

static int capture_num = 0;
static int buffer_num = 0;
static int infer_num = 0;
static int tone_num = 0;
static int awb_num = 0;
static int display_num = 0;

static float infer_time = 0.0;
static float tone_time = 0.0;
static float awb_time = 0.0;

static int pipe_run = 1;
static int capture_run = 1;
static int infer_run = 1;
static int tone_run = 1;
static int awb_run = 1;

static int save_run = 0;
static int exit_run = 0;
static char save_path[100];
static char save_path_jpg[100];
static char copy_path[100];

void *Inference_thread(void *threadid) {
	//auto stop_0 = high_resolution_clock::now(); 
	while(capture_run){
		    sem_wait(&InferSem);	
		    //if (Inference_count == 0){
				auto stop_0 = high_resolution_clock::now(); 
			//} 
			// Model parameter Copy
			CHECK(cudaMemcpy((float *)cuda_buffers[image_inputIndex], infer_buffer+infer_num*WIDTH*HEIGHT, WIDTH*HEIGHT*4, cudaMemcpyHostToDevice)); 				
			CHECK(cudaMemcpy(cuda_buffers[hidden_inputIndex],cuda_buffers[hidden_outputIndex], HEIGHT * WIDTH * 4,cudaMemcpyDeviceToDevice));
			// Model execute  
			context->execute(1, cuda_buffers);
			// Model result copy out
			
			if (Inference_count == 0){  
				CHECK(cudaMemcpy((float *)cuda_buffer_in, (float *)cuda_buffers[image_inputIndex],  WIDTH*HEIGHT*4, cudaMemcpyDeviceToDevice)); 
				CHECK(cudaMemcpy((float *)cuda_buffer_out,(float *)cuda_buffers[image_outputIndex], WIDTH*HEIGHT*4, cudaMemcpyDeviceToDevice));	 
			} 
			if (Inference_count > 0){			
				if (infer_num == tone_num){
					infer_num = tone_num - 1;
					if (infer_num == -1){
						infer_num = buffer_size-1;				
					}
				}
				CHECK(cudaMemcpy((float *)cuda_buffer_in + infer_num*WIDTH*HEIGHT, (float *)cuda_buffers[image_inputIndex],  WIDTH*HEIGHT*4, cudaMemcpyDeviceToDevice)); 
				CHECK(cudaMemcpy((float *)cuda_buffer_out+ infer_num*WIDTH*HEIGHT, (float *)cuda_buffers[image_outputIndex], WIDTH*HEIGHT*4, cudaMemcpyDeviceToDevice));	
			}
			Inference_count += 1;		
			
			auto stop_1 = high_resolution_clock::now();
			auto duration = duration_cast<microseconds>(stop_1 - stop_0);
			infer_time += duration.count()/1000000.0;
			float per_infer_time = infer_time / Inference_count;
			Inference_FPS = 1.0 / per_infer_time;	
			sem_getvalue(&ToneSem, &tone_value);
			if (tone_value < buffer_size){
				tone_value += 1;
				sem_post(&ToneSem);
			}	
			infer_num += 1;
			if (infer_num == buffer_size){
				infer_num = 0;
			}
	}	
	//cout << "Inference Done." << endl;
	infer_run = 0;
}

void compute_tone_curve(cv::Mat hist, cv::Mat sk){
	float chist_gpt_sum;
	Mat cgpt;
	pt = p0; 
	Mat hist_p;
	for (int ii =0;ii < N_iter; ii++){	
		chist_gpt_sum = 0.0;
		cgpt = (hist>pt);
		pow(hist, -1.0, hist_p);
						
		for (int jj = 0;jj<NBIN;jj++){
			if (cgpt.at<bool>(jj,0) == true){
				chist_gpt_sum += hist_p.at<float>(jj,0);
			}
		}				
		pt = (countNonZero(cgpt) - rdelta)/chist_gpt_sum;
	}
	chist_gpt_sum = 0.0;		
	cgpt = (hist>pt);	
	pow(hist, -1.0, hist_p);	
	for (int jj = 0;jj<NBIN;jj++){
		if (cgpt.at<bool>(jj,0) == true){
			chist_gpt_sum += hist_p.at<float>(jj,0);			
		}
	}
	for (int jj = 0;jj<NBIN;jj++){
		if (cgpt.at<bool>(jj,0) == true){
			sk.at<float>(0,jj) = 1.0+(rdelta - countNonZero(cgpt))/(hist.at<float>(jj,0)*chist_gpt_sum);
		}
	}		
}
void compute_vk(cv::Mat sk, cv::Mat vk){
	vk.at<float>(0,0)  = -r;
	for (int jj = 0;jj<NBIN;jj++){
		if (jj == 0){
			vk.at<float>(0,jj+1) = deltaHist*sk.at<float>(0,jj);
		}
		if (jj > 0){
			vk.at<float>(0,jj+1) =deltaHist*sk.at<float>(0,jj) + vk.at<float>(0,jj);
		}
	}
	for (int jj = 1;jj<NBIN+1;jj++){
		vk.at<float>(0,jj) = vk.at<float>(0,jj) - r;
	}
} 
void GAMMA_SV(cv::Mat sk, cv::Mat vk){	
	for (int jj = 0;jj<NBIN+1;jj++){
		vk.at<float>(0,jj) *= GAMMA;
	}
	for (int jj = 0;jj<NBIN;jj++){
		sk.at<float>(0,jj) *= GAMMA;
	}
} 
void hist_initial(cv::Mat hist_now){
	float hist_base = 1.0 / NBIN;
	for (int i = 0; i < NBIN; i++){
		hist_now.at<float>(i,0) = 0.8 * hist_base + 0.2 * hist_now.at<float>(i,0);
	}			
}
void *Tone_thread(void *threadid) { 
	//auto stop_0 = high_resolution_clock::now();	
	while(infer_run){
		Mat sk0  = cv::Mat::zeros(1, NBIN, CV_32FC1);
		Mat vk0  = cv::Mat::zeros(1, NBIN+1, CV_32FC1);
		sem_wait(&ToneSem);	
		//if (Tone_count == 0){
			auto stop_0 = high_resolution_clock::now(); 
		//}   
		// Demosaic, Luma, Log
		demosaicWrapper((float *)cuda_buffer_in+tone_num*WIDTH*HEIGHT, (float *)cuda_buffer_out+tone_num*WIDTH*HEIGHT,(float *)de_img,(float *)output_buffer,(float *)log_buffer); 
	    // Copy log(luma) out
	    CHECK(cudaMemcpy(log_img,(float *)log_buffer,HEIGHT*WIDTH*4, cudaMemcpyDeviceToHost)); //  has to be sync	
		// Base-Detail Decomposition
		Mat log_image(Size(WIDTH, HEIGHT), CV_32FC1, log_img, Mat::AUTO_STEP); 
		Mat image_Gaussian;
		GaussianBlur(log_image,image_Gaussian,Size(5,5),0.7,0.7); 
		memcpy(Gaussian_img,image_Gaussian.ptr<float>(),HEIGHT*WIDTH*4);
		CHECK(cudaMemcpyAsync(base_buffer, Gaussian_img, HEIGHT*WIDTH*4, cudaMemcpyHostToDevice)); 
		// Hist
	    Mat hist;
		calcHist( &image_Gaussian, 1, 0, cv::Mat(), hist, 1, histSize, ranges, true, false ); 
		//Mat Low_Light = hist.rowRange(3,10) + 10000;
		//Low_Light.copyTo(hist.rowRange(3,10));
		hist = hist / (sum(hist)[0]);
		if (Tone_count == 0){
			hist_initial(hist);
			memcpy(hist_buffer,hist.ptr<float>(),NBIN * sizeof(float));
			compute_tone_curve(hist, sk0);
		}
		if (Tone_count > 0){
			int i = Tone_count%2;
			Mat new_hist;
			if (i == 1){ // Current 1, pre 0
				Mat pre_hist(Size(1, NBIN), CV_32FC1, hist_buffer, Mat::AUTO_STEP);
				new_hist = 0.95*pre_hist + 0.05*hist;
				//new_hist = hist;
				memcpy(hist_buffer+NBIN*4,new_hist.ptr<float>(),NBIN * sizeof(float));
			}
			if (i == 0){ // Current 0, pre 1
				Mat pre_hist(Size(1, NBIN), CV_32FC1, hist_buffer+NBIN*4, Mat::AUTO_STEP);
				new_hist = 0.95*pre_hist + 0.05*hist;
				//new_hist = hist;
				memcpy(hist_buffer,new_hist.ptr<float>(),NBIN * sizeof(float));
			}
			compute_tone_curve(new_hist, sk0);
		}
		// vk0
		if (Tone_count == 0){
			compute_vk(sk0,vk0);
			memcpy(sk_buffer,sk0.ptr<float>(),NBIN * sizeof(float));
			GAMMA_SV(sk0,vk0);
			CHECK(cudaMemcpy(sk_0,sk0.ptr<float>(), NBIN * sizeof(float),cudaMemcpyHostToDevice)); 
			CHECK(cudaMemcpy(vk_0,vk0.ptr<float>(), (NBIN+1) * sizeof(float),cudaMemcpyHostToDevice)); 
		}
		if (Tone_count > 0){
			int i = Tone_count%2;		
			Mat new_sk;
			if (i == 0){ // Current 0, pre 1
				Mat pre_sk(Size(NBIN, 1), CV_32FC1, sk_buffer+NBIN*4, Mat::AUTO_STEP);
				new_sk = 0.95*pre_sk +  0.05*sk0;
				//new_sk= sk0;
				memcpy(sk_buffer,new_sk.ptr<float>(),NBIN * sizeof(float)); 
			}
			if (i == 1){ // current 1, pre 0
				Mat pre_sk(Size(NBIN, 1), CV_32FC1, sk_buffer, Mat::AUTO_STEP);
				new_sk = 0.95*pre_sk +  0.05*sk0;
				//new_sk= sk0;
				memcpy(sk_buffer+NBIN*4,new_sk.ptr<float>(),NBIN * sizeof(float));
			}   
			compute_vk(new_sk,vk0);
			GAMMA_SV(new_sk,vk0);
			CHECK(cudaMemcpy(sk_0,new_sk.ptr<float>(), NBIN * sizeof(float),cudaMemcpyHostToDevice)); // Mat to GPU
			CHECK(cudaMemcpy(vk_0,vk0.ptr<float>(), (NBIN+1) * sizeof(float),cudaMemcpyHostToDevice)); // Mat to GPU
		}
		// Apply tone curve
		toneWrapper((unsigned char *)output_buffer8T, (float *)output_buffer,(float *)log_buffer,(float *)base_buffer,(float *)sk_0,(float *)vk_0);	
		
		if (Tone_count == 0){  
			CHECK(cudaMemcpy(img_bgr8T,(unsigned char *)output_buffer8T,HEIGHT*WIDTH*3, cudaMemcpyDeviceToHost)); 
		} 
		if (Tone_count > 0){			
			if (tone_num == awb_num){
				tone_num = awb_num - 1;
				if (tone_num == -1){
					tone_num = buffer_size -1;				
				}
			}
			CHECK(cudaMemcpy(img_bgr8T + tone_num*3*WIDTH*HEIGHT,(unsigned char *)output_buffer8T,HEIGHT*WIDTH*3, cudaMemcpyDeviceToHost)); 	
		}
		
	    auto stop_1 = high_resolution_clock::now();
		auto duration = duration_cast<microseconds>(stop_1 - stop_0);
		Tone_count += 1;
		tone_time += duration.count()/1000000.0;
		float per_tone_time = tone_time / Tone_count;
		Tone_FPS = 1.0 / per_tone_time;
		
		sem_getvalue(&AWBSem, &awb_value);
		if (awb_value < buffer_size){
			awb_value += 1;
			sem_post(&AWBSem); 
		}	
		tone_num += 1;
		if (tone_num == buffer_size){
			tone_num = 0;
		}
	}
	//cout << "Tone Done." << endl;
	tone_run = 0;
}
 
void *AWB_thread(void *threadid) {
	//auto stop_0 = high_resolution_clock::now();
	char buf[100]; 
	//VideoWriter oVideoWriter("/home/blink/Videos/blink.avi", VideoWriter::fourcc('H', '2', '6', '4'), 30, Size(WIDTH,HEIGHT), true);
	while(tone_run){	
		sem_wait(&AWBSem);	
		//if (AWB_count == 0){
			auto stop_0 = high_resolution_clock::now(); 
		//}  
		//auto save_start = high_resolution_clock::now();
		// Mapped 8-bit BGR
		Mat image2M(Size(WIDTH, HEIGHT), CV_8UC3, img_bgr8T + awb_num*3*WIDTH*HEIGHT, Mat::AUTO_STEP);
		Mat image_1M,image_bgr;
		Mat img_0 = image2M.colRange(310, 1590);
		img_0.rowRange(60, 1020).copyTo(image_1M);
		resize(image_1M, image_bgr, Size(width, height), 0, 0, cv::INTER_AREA);

		// Split
		Mat bgr[3]; 
		split(image_bgr, bgr);
				
		double B_sum = sum(bgr[0])[0];
		double G_sum = sum(bgr[1])[0];
		double R_sum = sum(bgr[2])[0];
		Mat B2 = bgr[0].mul(bgr[0]);
		Mat R2 = bgr[2].mul(bgr[2]);
		double B2_sum = sum(B2)[0];
		double R2_sum = sum(R2)[0];
		double B_max,G_max,R_max;
		minMaxIdx(bgr[0],NULL,&B_max);
		minMaxIdx(bgr[1],NULL,&G_max);
		minMaxIdx(bgr[2],NULL,&R_max);
		double B2_max = B_max * B_max;
		double R2_max = R_max * R_max;
		double u_R = (G_sum*R_max - R_sum*G_max)  /(R2_sum*R_max - R_sum*R2_max);
		double v_R = (R2_sum*G_max - G_sum*R2_max)/(R2_sum*R_max - R_sum*R2_max);
		double u_B = (G_sum*B_max - B_sum*G_max)  /(B2_sum*B_max - B_sum*B2_max);
		double v_B = (B2_sum*G_max - G_sum*B2_max)/(B2_sum*B_max - B_sum*B2_max);
		Mat R0,R1,B0,B1;
		bgr[2].convertTo(R0, CV_8U, v_R,0.0);
		R2.convertTo(R1, CV_8U, u_R,0.0);
		Mat R_0 = R0+R1;
		bgr[0].convertTo(B0, CV_8U, v_B,0.0);
		B2.convertTo(B1, CV_8U, u_B,0.0);
		Mat B_0 = B0+B1;
		// Merge
		vector<Mat> channels;
		Mat image;
		channels.push_back(B_0);
		channels.push_back(bgr[1]);
		channels.push_back(R_0);
		merge(channels, image);
		if (save_run){
			sprintf(buf,"%s/Blink_%d.png",save_path_jpg,AWB_count); 
			imwrite(buf, image);			
		}
		if (AWB_count == 0){  
			memcpy(output_image,image.ptr<unsigned char>(),  3*height * width); 
		} 
		if (AWB_count > 0){			
			if (awb_num == display_num){
				awb_num = display_num - 1;
				if (awb_num == -1){
					awb_num = buffer_size - 1;				
				}
			} 
			memcpy(output_image + awb_num*3*width*height,image.ptr<unsigned char>(),  3*height * width);
		}	
	    auto stop_1 = high_resolution_clock::now();
		auto duration = duration_cast<microseconds>(stop_1 - stop_0);
		
		//auto save_stop = high_resolution_clock::now();
		//auto save_duration = duration_cast<microseconds>(save_stop - save_start);
		//cout << "Saving time: " << save_duration.count()/1000.0 << endl;
		AWB_count += 1;
		awb_time += duration.count()/1000000.0;
		float per_awb_time = awb_time / AWB_count;
		AWB_FPS = 1.0 / per_awb_time;	
		
		sem_getvalue(&DisplaySem, &display_value);
		if (display_value < buffer_size){
			display_value += 1;
			sem_post(&DisplaySem); 
		}	
		awb_num += 1;
		if (awb_num == buffer_size){
			awb_num = 0;
		}
	}
	//oVideoWriter.release();
	//cout << "AWB Done." << endl;
	awb_run = 0;
}

static void set_defaults(context_t * ctx)
{   
	//printf("context_t ctx initialization \n");
	//ctx->dec = NULL;  
    ctx->decoder_pixfmt = 1;
    ctx->crtc = 0;
    ctx->connector = 0;
    ctx->window_height = height;
    ctx->window_width = width;
    ctx->window_x = 0;
    ctx->window_y = 0; 
    ctx->streamHDR = false;
    struct drm_tegra_hdr_metadata_smpte_2086 drm_metadata;
    memset(&drm_metadata,0,sizeof(struct drm_tegra_hdr_metadata_smpte_2086));
    ctx->drm_renderer = NvDrmRenderer::createDrmRenderer("Display",ctx->window_width, ctx->window_height, ctx->window_x, ctx->window_y, ctx->connector, ctx->crtc, drm_metadata, ctx->streamHDR);
	//printf("Create drm_renderer\n");
	
}
void *Display_thread(void *threadid) {   
	set_defaults(&ctx_display);
	NvDrmFB ui_fb[1];
    (&ctx_display)->drm_renderer->createDumbFB(width, height,DRM_FORMAT_ARGB8888,&ui_fb[0]); 
    float display_time = 0.0; 
    char info_display_0[50];
    char info_display_1[50];
    auto stop_0 = high_resolution_clock::now(); 
    //char buf[50];	
    //char info_capture[50];	
    //char info_display[50]; 
    //char info_diff[50];
    //char info_0[50];
    //char info_1[50];
    //char info_2[50];
    //char info_3[50];
    //char info_4[50];
	while (awb_run)  
	{ 
		sem_wait(&DisplaySem);
		Mat image_out(Size(width, height), CV_8UC3, output_image+ display_num*3*width*height, Mat::AUTO_STEP);
		
		/*
		sprintf(info_capture,"Capture FPS: %.2f  Total Frame: %d",Capture_FPS,Capture_count);
		putText(image_out, //target image
            info_capture, //text
            cv::Point(0, 20), //top-left position
            cv::FONT_HERSHEY_DUPLEX,
            0.5,
            CV_RGB(255, 255, 255), //font color
            1);      
		sprintf(info_display,"Display FPS: %.2f  Display Frame: %d",Display_FPS,Display_count);
		putText(image_out, //target image
            info_display, //text
            cv::Point(0, 40), //top-left position
            cv::FONT_HERSHEY_DUPLEX,
            0.5,
            CV_RGB(255, 255, 255), //font color
            1);     
		sprintf(info_diff,"FPS Diff: %.2f  Frame Diff: %d (%.2f%)",Capture_FPS-Display_FPS,Capture_count-Display_count,(float)(Capture_count-Display_count)/Capture_count * 100.0);
		putText(image_out, //target image
            info_diff, //text
            cv::Point(0, 60), //top-left position
            cv::FONT_HERSHEY_DUPLEX,
            0.5,
            CV_RGB(255, 255, 255), //font color
            1);      
		sprintf(info_0,"Capture: %.2fms ",1.0/Capture_FPS*1000.0);
		putText(image_out, //target image
            info_0, //text
            cv::Point(0, 80), //top-left position
            cv::FONT_HERSHEY_DUPLEX,
            0.5,
            CV_RGB(255, 255, 255), //font color
            1);        
		sprintf(info_1,"Inference: %.2fms ",1.0/Inference_FPS*1000.0);
		putText(image_out, //target image
            info_1, //text
            cv::Point(0, 100), //top-left position
            cv::FONT_HERSHEY_DUPLEX,
            0.5,
            CV_RGB(255, 255, 255), //font color
            1);       
		sprintf(info_2,"Tone-mapping: %.2fms ",1.0/Tone_FPS*1000.0);
		putText(image_out, //target image
            info_2, //text
            cv::Point(0, 120), //top-left position
            cv::FONT_HERSHEY_DUPLEX,
            0.5,
            CV_RGB(255, 255, 255), //font color
            1);       
        sprintf(info_3,"AWB: %.2fms ",1.0/AWB_FPS*1000.0);
		putText(image_out, //target image
            info_3, //text
            cv::Point(0, 140), //top-left position
            cv::FONT_HERSHEY_DUPLEX,
            0.5,
            CV_RGB(255, 255, 255), //font color
            1);        
		sprintf(info_4,"Display: %.2fms ",1.0/Display_FPS*1000.0);
		putText(image_out, //target image
            info_4, //text
            cv::Point(0, 160), //top-left position
            cv::FONT_HERSHEY_DUPLEX,
            0.5,
            CV_RGB(255, 255, 255), //font color
            1);   
        */          
        if ((Display_count % 10 == 0) && (Display_count > 0)){
			auto stop_1 = high_resolution_clock::now();
			auto duration = duration_cast<microseconds>(stop_1 - stop_0);
			display_time = (duration.count())/(1000000.0*10);
			Display_FPS = 1.0 / display_time;
			stop_0 = high_resolution_clock::now();
		}
		sprintf(info_display_0,"FPS: %.2f ",Display_FPS);
		putText(image_out, //target image
           info_display_0, //text
           cv::Point(0, 25), //top-left position
           cv::FONT_HERSHEY_DUPLEX,
           1.0,
           CV_RGB(255, 255, 255), //font color
           2);
		sprintf(info_display_1,"Frame Counter: %d ",Display_count);
		putText(image_out, //target image
           info_display_1, //text
           cv::Point(0, 50), //top-left position
           cv::FONT_HERSHEY_DUPLEX,
           1.0,
           CV_RGB(255, 255, 255), //font color
           2);
        if (save_run == 0) // In preview, give save and exit instructions
        {
			char info_save_2[100];	
			sprintf(info_save_2,"Blink Previewing");
			putText(image_out, //target image
            info_save_2, //text
            cv::Point(0, 420), //top-left position
            cv::FONT_HERSHEY_DUPLEX,
            1,
            CV_RGB(255, 255, 255), //font color
            2);
			char info_save_0[100];	
			sprintf(info_save_0,"Press 's'+'enter' to start saving");
			putText(image_out, //target image
            info_save_0, //text
            cv::Point(0, 445), //top-left position
            cv::FONT_HERSHEY_DUPLEX,
            0.8,
            CV_RGB(255, 255, 255), //font color
            2);
            char info_save_1[100];	
			sprintf(info_save_1,"Press 'e'+'enter' to exit");
			putText(image_out, //target image
            info_save_1, //text
            cv::Point(0, 470), //top-left position
            cv::FONT_HERSHEY_DUPLEX,
            0.8,
            CV_RGB(255, 255, 255), //font color
            2);
		}
		if (save_run == 1) // In save, give quit and exit instructions
        {
			char info_save_2[100];	
			sprintf(info_save_2,"Blink Saving");
			putText(image_out, //target image
            info_save_2, //text
            cv::Point(0, 420), //top-left position
            cv::FONT_HERSHEY_DUPLEX,
            1,
            CV_RGB(255, 255, 255), //font color
            2);
			char info_save_0[100];	
			sprintf(info_save_0,"Press 'q'+'enter' to quit and back to preview");
			putText(image_out, //target image
            info_save_0, //text
            cv::Point(0, 445), //top-left position
            cv::FONT_HERSHEY_DUPLEX,
            0.8,
            CV_RGB(255, 255, 255), //font color
            2);
            char info_save_1[100];	
			sprintf(info_save_1,"Press 'e'+'enter' to exit");
			putText(image_out, //target image
            info_save_1, //text
            cv::Point(0, 470), //top-left position
            cv::FONT_HERSHEY_DUPLEX,
            0.8,
            CV_RGB(255, 255, 255), //font color
            2);
		}	
		Mat bgra;
		cv::cvtColor(image_out, bgra, cv::COLOR_BGR2BGRA);
		memcpy(bgra_out,bgra.ptr<unsigned char>(),  4*height * width);
		memcpy(ui_fb[0].bo[0].data, bgra_out, width*height*4);
		(&ctx_display)->drm_renderer->setPlane(0, ui_fb[0].fb_id,600, 200, width, height,0, 0, width << 16, height << 16);	
		Display_count += 1;
		//auto stop_1 = high_resolution_clock::now();
		//auto duration = duration_cast<microseconds>(stop_1 - stop_0);
		//display_time = (duration.count())/(1000000.0*Display_count);
		//Display_FPS = 1.0 / display_time;
		display_num += 1;
		if (display_num == buffer_size){
			display_num = 0;
		}
		
	} 	
	//cout << "Display Done." << endl;
}	

void *Default_thread(void *threadid) {   
	set_defaults(&ctx_display);
	NvDrmFB ui_fb[1];
    (&ctx_display)->drm_renderer->createDumbFB(width, height,DRM_FORMAT_ARGB8888,&ui_fb[0]); 
    auto stop_0 = high_resolution_clock::now(); 				
    float display_time = 0.0;
    char info_display_0[50];
    char info_display_1[50];
	while (capture_run)  
	{ 
		sem_wait(&DefaultSem);
		//if (Display_count == 0){
			//stop_0 = high_resolution_clock::now(); 
		//}  
		Mat image_out(Size(width, height), CV_8UC3, output_image_default+ display_num*3*width*height, Mat::AUTO_STEP);
        if ((Display_count % 10 == 0) && (Display_count > 0)){
			auto stop_1 = high_resolution_clock::now();
			auto duration = duration_cast<microseconds>(stop_1 - stop_0);
			display_time = (duration.count())/(1000000.0*10);
			Display_FPS = 1.0 / display_time;
			stop_0 = high_resolution_clock::now();
		}
		sprintf(info_display_0,"FPS: %.2f ",Display_FPS);
		putText(image_out, //target image
           info_display_0, //text
           cv::Point(0, 25), //top-left position
           cv::FONT_HERSHEY_DUPLEX,
           1.0,
           CV_RGB(255, 255, 255), //font color
           2);
		sprintf(info_display_1,"Frame Counter: %d ",Display_count);
		putText(image_out, //target image
           info_display_1, //text
           cv::Point(0, 50), //top-left position
           cv::FONT_HERSHEY_DUPLEX,
           1.0,
           CV_RGB(255, 255, 255), //font color
           2);  
		/*
		sprintf(info_capture,"Capture FPS: %.2f  Total Frame: %d",Capture_FPS,Capture_count);
		putText(image_out, //target image
            info_capture, //text
            cv::Point(0, 20), //top-left position
            cv::FONT_HERSHEY_DUPLEX,
            0.5,
            CV_RGB(255, 255, 255), //font color
            1);
        
		sprintf(info_display,"Display FPS: %.2f  Display Frame: %d",Display_FPS,Display_count);
		putText(image_out, //target image
            info_display, //text
            cv::Point(0, 40), //top-left position
            cv::FONT_HERSHEY_DUPLEX,
            0.5,
            CV_RGB(255, 255, 255), //font color
            1);
        
		sprintf(info_diff,"FPS Diff: %.2f  Frame Diff: %d (%.2f%)",Capture_FPS-Display_FPS,Capture_count-Display_count,(float)(Capture_count-Display_count)/Capture_count * 100.0);
		putText(image_out, //target image
            info_diff, //text
            cv::Point(0, 60), //top-left position
            cv::FONT_HERSHEY_DUPLEX,
            0.5,
            CV_RGB(255, 255, 255), //font color
            1);
        
		sprintf(info_0,"Capture: %.2fms ",1.0/Capture_FPS*1000.0);
		putText(image_out, //target image
            info_0, //text
            cv::Point(0, 80), //top-left position
            cv::FONT_HERSHEY_DUPLEX,
            0.5,
            CV_RGB(255, 255, 255), //font color
            1);
       
		sprintf(info_4,"Display: %.2fms ",1.0/Display_FPS*1000.0);
		putText(image_out, //target image
            info_4, //text
            cv::Point(0, 100), //top-left position
            cv::FONT_HERSHEY_DUPLEX,
            0.5,
            CV_RGB(255, 255, 255), //font color
            1);	
		*/
        if (save_run == 0) // In preview, give save and exit instructions
        {
			char info_save_2[100];	
			sprintf(info_save_2,"Default Previewing");
			putText(image_out, //target image
            info_save_2, //text
            cv::Point(0, 420), //top-left position
            cv::FONT_HERSHEY_DUPLEX,
            1,
            CV_RGB(255, 255, 255), //font color
            2);
			char info_save_0[100];	
			sprintf(info_save_0,"Press 's'+'enter' to start saving");
			putText(image_out, //target image
            info_save_0, //text
            cv::Point(0, 445), //top-left position
            cv::FONT_HERSHEY_DUPLEX,
            0.8,
            CV_RGB(255, 255, 255), //font color
            2);
            char info_save_1[100];	
			sprintf(info_save_1,"Press 'e'+'enter' to exit");
			putText(image_out, //target image
            info_save_1, //text
            cv::Point(0, 470), //top-left position
            cv::FONT_HERSHEY_DUPLEX,
            0.8,
            CV_RGB(255, 255, 255), //font color
            2);
		}
		if (save_run == 1) // In save, give quit and exit instructions
        {
			char info_save_2[100];	
			sprintf(info_save_2,"Default Saving");
			putText(image_out, //target image
            info_save_2, //text
            cv::Point(0, 420), //top-left position
            cv::FONT_HERSHEY_DUPLEX,
            1,
            CV_RGB(255, 255, 255), //font color
            2);
			char info_save_0[100];	
			sprintf(info_save_0,"Press 'q'+'enter' to quit and back to preview");
			putText(image_out, //target image
            info_save_0, //text
            cv::Point(0, 445), //top-left position
            cv::FONT_HERSHEY_DUPLEX,
            0.8,
            CV_RGB(255, 255, 255), //font color
            2);
            char info_save_1[100];	
			sprintf(info_save_1,"Press 'e'+'enter' to exit");
			putText(image_out, //target image
            info_save_1, //text
            cv::Point(0, 470), //top-left position
            cv::FONT_HERSHEY_DUPLEX,
            0.8,
            CV_RGB(255, 255, 255), //font color
            2);
		}

		Mat bgra;
		cv::cvtColor(image_out, bgra, cv::COLOR_BGR2BGRA);
		memcpy(bgra_out,bgra.ptr<unsigned char>(),  4*height * width);
		memcpy(ui_fb[0].bo[0].data, bgra_out, width*height*4);
		(&ctx_display)->drm_renderer->setPlane(0, ui_fb[0].fb_id,600, 200, width, height,0, 0, width << 16, height << 16);
		Display_count += 1;
		//auto stop_1 = high_resolution_clock::now(); 
		//auto duration = duration_cast<microseconds>(stop_1 - stop_0);
		//display_time = (duration.count())/(1000000.0*Display_count);
		//Display_FPS = 1.0 / display_time;
		display_num += 1;
		if (display_num == buffer_size){
			display_num = 0;
		}
		
	} 	
	cout << "Display Done." << endl;
}

void *Save_thread(void *threadid) { 
	char a;
    while (a != 's' && a != 'q' && a != 'e' ){
        a = getchar();
        if (a == 's'){
			pipe_run = 0;
			save_run = 1;
			exit_run = 0;
			//cout << "pipe: " << pipe_run << " save: " << save_run << endl;
			cout << "Switching to Save mode." << endl;
		}
		if (a == 'q'){
			pipe_run = 0;
			save_run = 0;
			exit_run = 0;
			//cout << "pipe: " << pipe_run << " save: " << save_run << endl;
			cout << "Switching back to Preview mode." << endl;
		} 
		if (a == 'e'){
			pipe_run = 0;
			save_run = 0;
			exit_run = 1;
			//cout << "pipe: " << pipe_run << " exit: " << exit_run << endl;
			cout << "Closing the pipeline." << endl;
		}	
    }
}
void para_init()
{
	pipe_run = 1;
	capture_run = 1;
	infer_run = 1;
	tone_run = 1;
	awb_run = 1;
	
	Capture_count = 0;
	Tone_count = 0;
	AWB_count = 0;
	Inference_count = 0;
	Display_count = 0;
	
	infer_time = 0.0;
	tone_time = 0.0;
	awb_time = 0.0;

	capture_num = 0;
	buffer_num = 0;
	infer_num = 0;
	tone_num = 0;
	awb_num = 0;
	display_num = 0;
}
	
int main(int argc, char **argv)  try
{
	string str_blink = "blink";
	string str_default = "default";
	string str_auto = "auto";
	string str_manual = "manual";
	namespace fs = std::experimental::filesystem;
	if (argv[2] == str_auto)
	{ 
		auto_exposure = 1;
	}
	if (argv[2] == str_manual)
	{
		auto_exposure = 0;	
	} 
	
	if (argv[1] == str_default) // if in default mode
    {
		
		CHECK(cudaMallocHost((void **)&output_image_default,buffer_size*height*width*3));
		CHECK(cudaMallocHost((void **)&bgra_out,height*width*4));
		while(!exit_run){
			// Firstly, start preview pipeline
			para_init();
			rs2::context ctx;
			auto list = ctx.query_devices(); // Get a snapshot of currently connected devices
			if (list.size() == 0){
				throw std::runtime_error("No device detected. Is it plugged in?");
			}
			rs2::pipeline pipe_default;
			rs2::config cfg;  
			cfg.enable_stream(RS2_STREAM_COLOR,width,height, RS2_FORMAT_BGR8,30);
			if (auto_exposure == 1){
				cout << "Default mode is running." << endl;
				cout << "Auto-Exposure is running." << endl;
				rs2::pipeline_profile selection_0 = pipe_default.start(cfg); 
				rs2::device selected_device_0 = selection_0.get_device();
				auto color_sensor_0 = selected_device_0.first<rs2::color_sensor>();
				color_sensor_0.set_option(RS2_OPTION_ENABLE_AUTO_EXPOSURE, 0); 
				color_sensor_0.set_option(RS2_OPTION_EXPOSURE, exposure_time); // 33ms
				color_sensor_0.set_option(RS2_OPTION_GAIN, 64); 
				color_sensor_0.set_option(RS2_OPTION_SATURATION, 50); 
				color_sensor_0.set_option(RS2_OPTION_SHARPNESS, 50); 
				color_sensor_0.set_option(RS2_OPTION_ENABLE_AUTO_WHITE_BALANCE, 1);
				color_sensor_0.set_option(RS2_OPTION_BACKLIGHT_COMPENSATION, 0);
				color_sensor_0.set_option(RS2_OPTION_ENABLE_AUTO_EXPOSURE, 1); 
				color_sensor_0.set_option(RS2_OPTION_AUTO_EXPOSURE_PRIORITY, 1); 
			}
			if (auto_exposure == 0){
				cout << "Default mode is running." << endl;
				cout << "Manual-Exposure is running." << endl;
				rs2::pipeline_profile selection = pipe_default.start(cfg);
				rs2::device selected_device = selection.get_device();
				auto color_sensor = selected_device.first<rs2::color_sensor>();
				color_sensor.set_option(RS2_OPTION_ENABLE_AUTO_EXPOSURE, 0);
				color_sensor.set_option(RS2_OPTION_EXPOSURE, exposure_time); // 33ms
				color_sensor.set_option(RS2_OPTION_GAIN, 128); // 128 gain
				color_sensor.set_option(RS2_OPTION_SATURATION, 50); 
				color_sensor.set_option(RS2_OPTION_SHARPNESS, 50); 
				color_sensor.set_option(RS2_OPTION_BACKLIGHT_COMPENSATION, 0); 
				color_sensor.set_option(RS2_OPTION_ENABLE_AUTO_WHITE_BALANCE, 1);
			}
			sem_init(&DefaultSem, 0, 0);
			pthread_t threads[2];
			int rc;
			//printf("Creating Default thread\n");
			rc = pthread_create(&threads[0], NULL, Default_thread, (void *) 0);
			if (rc) {
				cout << "Error:unable to create Default thread," << rc << endl;
				exit(-1);
			}
			//printf("Creating Function thread\n");
			rc = pthread_create(&threads[1], NULL, Save_thread, (void *) 1);
			if (rc) {
				cout << "Error:unable to create Function thread," << rc << endl;
				exit(-1);
			}
			char buf[100];
			for (auto i = 0; i < 30; ++i) pipe_default.wait_for_frames(); 
			auto stop_0 = high_resolution_clock::now(); 
			while (pipe_run)
			{
				if (Capture_count == 0){
					stop_0 = high_resolution_clock::now();
				}
				rs2::frameset data_default = pipe_default.wait_for_frames(); // Wait for next set of frames from the camera
				data_default.keep();
				rs2::frame depth = data_default.get_color_frame();       // get color frame      
				Mat image = Mat( height, width, CV_8UC3, const_cast<void*>( depth.get_data() ) );
				if (Capture_count == 0){  
					memcpy(output_image_default,image.ptr<unsigned char>(),  3*height * width); 		 
				} 
				if (Capture_count > 0){			
					if (capture_num == display_num){
						capture_num = display_num - 1;
						if (capture_num == -1){
							capture_num = buffer_size -1;				
						}
					}
					memcpy(output_image_default + capture_num*3*width*height,image.ptr<unsigned char>(),  3*height * width);
				} 		
				auto stop_1 = high_resolution_clock::now();
				auto duration = duration_cast<microseconds>(stop_1 - stop_0);
				Capture_count += 1;
				capture_num += 1;
				if (capture_num == buffer_size){
					capture_num = 0;				
				}
				float capture_time = (duration.count())/(1000000.0 * Capture_count);
				Capture_FPS = 1.0 / capture_time;
				sem_getvalue(&DefaultSem, &default_value);
				if (default_value < buffer_size){
					default_value += 1;
					sem_post(&DefaultSem);
				}	
			}
			//cout << "Capture Done." << endl;
			capture_run = 0;
			(void) pthread_join(threads[1], NULL);
			(void) pthread_join(threads[0], NULL);
			pipe_default.stop(); 
			
			if (save_run == 1)
			{
				para_init();
				rs2::context ctx;
				auto list = ctx.query_devices(); // Get a snapshot of currently connected devices
				if (list.size() == 0){
					throw std::runtime_error("No device detected. Is it plugged in?");
				}
				time_t rawtime;
				struct tm * timeinfo;
				time (&rawtime);
				timeinfo = localtime(&rawtime);
				strftime(save_path,sizeof(save_path),"/home/blink/Documents/%m_%d_%Y_%H_%M_default",timeinfo);	
				strftime(copy_path,sizeof(copy_path),"/media/blink/BlinkSSD/%m_%d_%Y_%H_%M_default",timeinfo);	
				string str(save_path);
				string bag_name;
				bag_name = str+"_BGR8.bag";
				string str_copy(copy_path);
				string bag_name_copy;
				bag_name_copy = str_copy + "_BGR8.bag";
				rs2::pipeline pipe_default;
				rs2::config cfg;   
				cfg.enable_record_to_file(bag_name_copy); // Start to save RAW 
				cfg.enable_stream(RS2_STREAM_COLOR,width,height, RS2_FORMAT_BGR8,30);
				
				if (auto_exposure == 1){
					rs2::pipeline_profile selection_0 = pipe_default.start(cfg); 
					rs2::device selected_device_0 = selection_0.get_device();
					auto color_sensor_0 = selected_device_0.first<rs2::color_sensor>();
					color_sensor_0.set_option(RS2_OPTION_ENABLE_AUTO_EXPOSURE, 0); 
					color_sensor_0.set_option(RS2_OPTION_EXPOSURE, exposure_time); // 33ms
					color_sensor_0.set_option(RS2_OPTION_GAIN, 64); 
					color_sensor_0.set_option(RS2_OPTION_SATURATION, 50); 
					color_sensor_0.set_option(RS2_OPTION_SHARPNESS, 50); 
					color_sensor_0.set_option(RS2_OPTION_ENABLE_AUTO_WHITE_BALANCE, 1);
					color_sensor_0.set_option(RS2_OPTION_BACKLIGHT_COMPENSATION, 0);
					color_sensor_0.set_option(RS2_OPTION_ENABLE_AUTO_EXPOSURE, 1); 
					color_sensor_0.set_option(RS2_OPTION_AUTO_EXPOSURE_PRIORITY, 1); 
				}
				if (auto_exposure == 0){
					rs2::pipeline_profile selection = pipe_default.start(cfg);
					rs2::device selected_device = selection.get_device();
					auto color_sensor = selected_device.first<rs2::color_sensor>();
					color_sensor.set_option(RS2_OPTION_ENABLE_AUTO_EXPOSURE, 0);
					color_sensor.set_option(RS2_OPTION_EXPOSURE, exposure_time); // 33ms
					color_sensor.set_option(RS2_OPTION_GAIN, 128); // 128 gain
					color_sensor.set_option(RS2_OPTION_SATURATION, 50); 
					color_sensor.set_option(RS2_OPTION_SHARPNESS, 50); 
					color_sensor.set_option(RS2_OPTION_BACKLIGHT_COMPENSATION, 0);
					color_sensor.set_option(RS2_OPTION_ENABLE_AUTO_WHITE_BALANCE, 1);
				}
				sem_init(&DefaultSem, 0, 0);
				pthread_t threads[2];
				int rc;
				//printf("Creating Default thread\n");
				rc = pthread_create(&threads[0], NULL, Default_thread, (void *) 0);
				if (rc) {
					cout << "Error:unable to create Default thread," << rc << endl;
					exit(-1);
				}
				//printf("Creating Function thread\n");
				rc = pthread_create(&threads[1], NULL, Save_thread, (void *) 1);
				if (rc) {
					cout << "Error:unable to create Function thread," << rc << endl;
					exit(-1);
				}
				char buf[100];
				//for (auto i = 0; i < 30; ++i) pipe_default.wait_for_frames(); 
				auto stop_0 = high_resolution_clock::now(); 
				while (pipe_run)
				{
					if (Capture_count == 0){
						stop_0 = high_resolution_clock::now();
					}
					rs2::frameset data_default = pipe_default.wait_for_frames(); // Wait for next set of frames from the camera
					data_default.keep();
					rs2::frame depth = data_default.get_color_frame();       // get color frame       
					Mat image = Mat( height, width, CV_8UC3, const_cast<void*>( depth.get_data() ) );
					if (Capture_count == 0){  
						memcpy(output_image_default,image.ptr<unsigned char>(),  3*height * width); 		 
					} 
					if (Capture_count > 0){			
						if (capture_num == display_num){
							capture_num = display_num - 1;
							if (capture_num == -1){
								capture_num = buffer_size -1;				
							}
						}
						memcpy(output_image_default + capture_num*3*width*height,image.ptr<unsigned char>(),  3*height * width);
					} 		
					auto stop_1 = high_resolution_clock::now();
					auto duration = duration_cast<microseconds>(stop_1 - stop_0);
					Capture_count += 1;
					capture_num += 1;
					if (capture_num == buffer_size){
						capture_num = 0;				
					}
					float capture_time = (duration.count())/(1000000.0 * Capture_count);
					Capture_FPS = 1.0 / capture_time;
					sem_getvalue(&DefaultSem, &default_value);
					if (default_value < buffer_size){
						default_value += 1;
						sem_post(&DefaultSem);
					}	
				}
				//cout << "Capture Done." << endl;
				capture_run = 0;
				(void) pthread_join(threads[1], NULL);
				(void) pthread_join(threads[0], NULL);
				pipe_default.stop(); 
				//fs::copy(bag_name, bag_name_copy);
			}			
		}
		CHECK(cudaFreeHost(output_image_default));
		CHECK(cudaFreeHost(bgra_out));
	}
		
	if (argv[1] == str_blink) // if in blink mode
	{
		// create a model using the API directly and serialize it to a stream
		//printf("Create a model using the API directly and serialize it to a stream. Line %d.\n",__LINE__);
		IHostMemory* modelStream{nullptr};
		ICudaEngine* cuda_engine(nullptr);
		IRuntime* runtime = createInferRuntime(gLogger);
		assert(runtime != nullptr);
		if(FROM_DISK){
			string enginePath{"/home/blink/realtimeV3/model/lowlightV1.engine"};
			//printf("Load engine. Line %d.\n",__LINE__);
			string buffer = readBuffer(enginePath);
			//printf("deserializeCudaEngine. Line %d.\n",__LINE__);
			cuda_engine = runtime->deserializeCudaEngine(buffer.data(), buffer.size(), nullptr);
			//printf("assert cuda_engine. Line %d.\n",__LINE__);
			assert(cuda_engine != nullptr);
		}
		else{
			//printf("Build engine from weights. Line %d.\n",__LINE__);
			IHostMemory* modelStream{nullptr};
			APIToModel(1, &modelStream);
			assert(modelStream != nullptr);
			//printf("deserializeCudaEngine. Line %d.\n",__LINE__);
			cuda_engine = runtime->deserializeCudaEngine(modelStream->data(), modelStream->size(), nullptr);
			//printf("assert cuda_engine. Line %d.\n",__LINE__);
			assert(cuda_engine != nullptr);
			modelStream->destroy();
		}
		//printf("cuda_engine done. Line %d.\n",__LINE__);
		// Read random digit file
		srand(unsigned(time(nullptr))); // set the starting value (seed) for generating a sequence of pseudo-random integer values.
		//printf("createExecutionContext. Line %d.\n",__LINE__);
		context = cuda_engine->createExecutionContext();
		assert(context != nullptr);     
		//printf("getNbBindings. Line %d.\n",__LINE__);
		const ICudaEngine& engine = context->getEngine(); // Get the number of binding indices.
		assert(engine.getNbBindings() == 4); // input 2, output 2   
		int batchSize = 1;
		// In order to bind the buffers, we need to know the names of the input and output tensors.
		// Note that indices are guaranteed to be less than IEngine::getNbBindings()
		//printf("getBindingIndex. Line %d.\n",__LINE__);
		image_inputIndex = engine.getBindingIndex(INPUT_BLOB_IMAGE_NAME);
		hidden_inputIndex = engine.getBindingIndex(INPUT_BLOB_HIDDEN_NAME);
		image_outputIndex = engine.getBindingIndex(OUTPUT_BLOB_IMAGE_NAME); 
		hidden_outputIndex = engine.getBindingIndex(OUTPUT_BLOB_HIDDEN_NAME);
		// Create GPU buffers on device
		//printf("Allocating buffers. Line %d.\n",__LINE__);
		CHECK(cudaMalloc(&cuda_buffers[image_inputIndex]  , batchSize * HEIGHT * WIDTH * sizeof(float)));
		CHECK(cudaMalloc(&cuda_buffers[hidden_inputIndex] , batchSize * HEIGHT * WIDTH * sizeof(float)));
		CHECK(cudaMalloc(&cuda_buffers[image_outputIndex] , batchSize * HEIGHT * WIDTH * sizeof(float)));
		CHECK(cudaMalloc(&cuda_buffers[hidden_outputIndex], batchSize * HEIGHT * WIDTH * sizeof(float)));
		CHECK(cudaMalloc(&norm_input, batchSize * HEIGHT * WIDTH * sizeof(float)));
		CHECK(cudaMalloc(&de_img,  batchSize * HEIGHT * WIDTH *   sizeof(float)));
		CHECK(cudaMalloc((void **) &input_buffer, batchSize * HEIGHT * WIDTH * sizeof(unsigned short)));
		CHECK(cudaMalloc(&output_buffer, batchSize * 3 * HEIGHT * WIDTH * 4));
		CHECK(cudaMalloc(&output_buffer8T, batchSize * 3 * HEIGHT * WIDTH));
		CHECK(cudaMalloc(&log_buffer, batchSize  * HEIGHT * WIDTH * 4));
		CHECK(cudaMalloc(&base_buffer, batchSize  * HEIGHT * WIDTH * 4));
		CHECK(cudaMalloc(&cuda_buffer_in,  batchSize * buffer_size*HEIGHT * WIDTH *   sizeof(float)));
		CHECK(cudaMalloc(&cuda_buffer_out,  batchSize * buffer_size*HEIGHT * WIDTH *   sizeof(float)));
		CHECK(cudaMalloc(&sk_0, batchSize  * NBIN * 4));
		CHECK(cudaMalloc(&vk_0, batchSize  * (NBIN+1) * 4));
		// Create stream
		//printf("Create CUDA stream. Line %d.\n",__LINE__);
		cudaStream_t stream;
		CHECK(cudaStreamCreate(&stream));
		CHECK(cudaMallocHost((void **)&output_image,buffer_size*height*width*3));
		CHECK(cudaMallocHost((void **)&bgra_out,height*width*4));
		CHECK(cudaMallocHost((void **)&img_bgr8T,buffer_size*HEIGHT*WIDTH*3));
		CHECK(cudaMallocHost((void **)&log_img,HEIGHT*WIDTH*4));
		CHECK(cudaMallocHost((void **)&cuda_in,HEIGHT*WIDTH*4));
		CHECK(cudaMallocHost((void **)&cuda_out,HEIGHT*WIDTH*4));
		CHECK(cudaMallocHost((void **)&Gaussian_img,HEIGHT*WIDTH*4));      
		CHECK(cudaMallocHost((void **)&img_input_buffer,buffer_size*HEIGHT*WIDTH*2));
		CHECK(cudaMallocHost((void **)&infer_buffer,buffer_size*HEIGHT*WIDTH*4));
		CHECK(cudaMallocHost((void **)&sk_buffer,2*NBIN*4)); 
		CHECK(cudaMallocHost((void **)&hist_buffer,2*NBIN*4));
		Mat hidden = Mat::zeros(HEIGHT, WIDTH, CV_32FC1);
		CHECK(cudaMemcpy(cuda_buffers[hidden_inputIndex],hidden.ptr<float>(), HEIGHT * WIDTH * sizeof(float),cudaMemcpyHostToDevice));
		CHECK(cudaMemcpy(cuda_buffers[hidden_outputIndex],hidden.ptr<float>(),HEIGHT * WIDTH * sizeof(float),cudaMemcpyHostToDevice));
		
		while(!exit_run){
			// Firstly, start preview pipeline
			para_init();
			rs2::context ctx;
			auto list = ctx.query_devices(); // Get a snapshot of currently connected devices
			if (list.size() == 0){
				throw std::runtime_error("No device detected. Is it plugged in?");
			}
			rs2::pipeline pipe;
			rs2::config cfg;  
			cfg.enable_stream(RS2_STREAM_COLOR,1920,1080, RS2_FORMAT_RAW16);
			if (auto_exposure == 1){
				cout << "Blink mode is running." << endl;
				cout << "Auto-Exposure is running." << endl;
				rs2::pipeline_profile selection_0 = pipe.start(cfg); 
				rs2::device selected_device_0 = selection_0.get_device();
				auto color_sensor_0 = selected_device_0.first<rs2::color_sensor>();
				color_sensor_0.set_option(RS2_OPTION_ENABLE_AUTO_EXPOSURE, 0); 
				color_sensor_0.set_option(RS2_OPTION_EXPOSURE, exposure_time); // 33ms
				color_sensor_0.set_option(RS2_OPTION_GAIN, 64); 
				color_sensor_0.set_option(RS2_OPTION_SATURATION, 50); 
				color_sensor_0.set_option(RS2_OPTION_SHARPNESS, 50); 
				color_sensor_0.set_option(RS2_OPTION_ENABLE_AUTO_WHITE_BALANCE, 1);
				color_sensor_0.set_option(RS2_OPTION_BACKLIGHT_COMPENSATION, 0);
				color_sensor_0.set_option(RS2_OPTION_ENABLE_AUTO_EXPOSURE, 1); 
				color_sensor_0.set_option(RS2_OPTION_AUTO_EXPOSURE_PRIORITY, 1);
				
			}
			if (auto_exposure == 0){
				cout << "Blink mode is running." << endl;
				cout << "Manual-Exposure is running." << endl;
				rs2::pipeline_profile selection = pipe.start(cfg);
				rs2::device selected_device = selection.get_device();
				auto color_sensor = selected_device.first<rs2::color_sensor>();
				color_sensor.set_option(RS2_OPTION_ENABLE_AUTO_EXPOSURE, 0);
				color_sensor.set_option(RS2_OPTION_EXPOSURE, exposure_time); // 33ms
				color_sensor.set_option(RS2_OPTION_GAIN, 128); // 128 gain
				color_sensor.set_option(RS2_OPTION_SATURATION, 50); 
				color_sensor.set_option(RS2_OPTION_SHARPNESS, 50); 
				color_sensor.set_option(RS2_OPTION_BACKLIGHT_COMPENSATION, 0);
				color_sensor.set_option(RS2_OPTION_ENABLE_AUTO_WHITE_BALANCE, 1);
			}
			sem_init(&InferSem, 0, 0);
			sem_init(&ToneSem, 0, 0);
			sem_init(&AWBSem, 0, 0);
			sem_init(&DisplaySem, 0, 0);
			pthread_t threads[5];
			int rc;
			//printf("Creating Inference thread\n");
			rc = pthread_create(&threads[0], NULL, Inference_thread, (void *) 0);
			if (rc) {
				cout << "Error:unable to create Inference thread," << rc << endl;
				exit(-1);
			}
			//printf("Creating Tone thread\n");
			rc = pthread_create(&threads[1], NULL, Tone_thread, (void *) 1);
			if (rc) {
				cout << "Error:unable to create Tone thread," << rc << endl;
				exit(-1);
			}
			//printf("Creating AWB thread\n");
			rc = pthread_create(&threads[2], NULL, AWB_thread, (void *) 2);
			if (rc) {
				cout << "Error:unable to create AWB thread," << rc << endl;
				exit(-1);
			}
			//printf("Creating Display thread\n");
			rc = pthread_create(&threads[3], NULL, Display_thread, (void *) 3);
			if (rc) {
				cout << "Error:unable to create Display thread," << rc << endl;
				exit(-1);
			}    
			//printf("Creating Save thread\n");
			rc = pthread_create(&threads[4], NULL, Save_thread, (void *) 4);
			if (rc) {
				cout << "Error:unable to create Save thread," << rc << endl;
				exit(-1);
			}
			for (auto i = 0; i < 30; ++i) pipe.wait_for_frames(); //  Capture 30 frames to give autoexposure, etc. a chance to settle 
			auto stop_0 = high_resolution_clock::now(); 
			while (pipe_run)
			{
				if (Capture_count == 0){
					stop_0 = high_resolution_clock::now();
				}
				rs2::frameset data = pipe.wait_for_frames(); // Wait for next set of frames from the camera
				//data.keep();
				// Obtain RAW frame
				rs2::frame depth = data.get_color_frame();       // get color frame       
				Mat image(Size(1920, 1080), CV_16UC1, (void*)depth.get_data(),Mat::AUTO_STEP);    
				memcpy(img_input_buffer+capture_num*WIDTH*HEIGHT, image.ptr<ushort>(), WIDTH*HEIGHT*2);		
				CHECK(cudaMemcpy(input_buffer, img_input_buffer+capture_num*WIDTH*HEIGHT, WIDTH*HEIGHT*2, cudaMemcpyHostToDevice)); // Mat image to GPU
				// Input Pre-process
				imgNormalizeWrapper((ushort *)input_buffer,(float *)norm_input);		
					
				if (Capture_count == 0){  
					CHECK(cudaMemcpy(infer_buffer, (float *)norm_input, WIDTH*HEIGHT*4, cudaMemcpyDeviceToHost));		 
				} 
				if (Capture_count > 0){			
					if (buffer_num == infer_num){
						buffer_num = infer_num - 1;
						if (buffer_num == -1){
							buffer_num = buffer_size -1;				
						}
					}
					CHECK(cudaMemcpy(infer_buffer+buffer_num*WIDTH*HEIGHT, (float *)norm_input, WIDTH*HEIGHT*4, cudaMemcpyDeviceToHost));
				} 		
				auto stop_1 = high_resolution_clock::now();
				auto duration = duration_cast<microseconds>(stop_1 - stop_0);
				Capture_count += 1;
				buffer_num += 1;
				capture_num += 1;
				if (buffer_num == buffer_size){
					buffer_num = 0;				
				}
				if (capture_num == buffer_size){
					capture_num = 0;
				}
				float capture_time = (duration.count())/(1000000.0 * Capture_count);
				Capture_FPS = 1.0 / capture_time;
				sem_getvalue(&InferSem, &infer_value);
				if (infer_value < buffer_size){
					infer_value += 1;
					sem_post(&InferSem);
				}	
			}   
			// if press e(exit) button, exit_run = 1, !exit_run = 0, jump out while(!exit) loop
			// if press q(quit) button, exit_run = 0, keep in this loop, restart the preview pipeline
			// if press s (save) button , pipe_run = 0(pipeline stops), save_run = 1 (save_data), shut down current pipeline and start a new one with saving function
			//cout << "Capture Done." << endl;
			capture_run = 0;
			(void) pthread_join(threads[4], NULL);
			(void) pthread_join(threads[0], NULL);
			(void) pthread_join(threads[1], NULL);
			(void) pthread_join(threads[2], NULL);
			(void) pthread_join(threads[3], NULL);
			pipe.stop(); 

			if (save_run)
			{
				para_init();
				rs2::context ctx;
				auto list = ctx.query_devices(); // Get a snapshot of currently connected devices
				if (list.size() == 0){
					throw std::runtime_error("No device detected. Is it plugged in?");
				}
				// Get current time and create a folder in documents. Save processed jpg into this folder.
				// Save the .bag file with RAW16 in Documents folder which name equals the current time
				time_t rawtime;
				struct tm * timeinfo;
				time (&rawtime);
				timeinfo = localtime(&rawtime);
				strftime(save_path,sizeof(save_path),"/media/blink/BlinkSSD/%m_%d_%Y_%H_%M_blink",timeinfo);	
				strftime(save_path_jpg,sizeof(save_path_jpg),"/media/blink/BlinkSSD/%m_%d_%Y_%H_%M_blink",timeinfo);
				if (mkdir(save_path_jpg, 0777) == -1) 
				{
					cerr << "Error :  " << strerror(errno) << endl; 
				}
				else
				{
					cout << "Directory created" << endl; 
				}
				string str(save_path);
				string bag_name;
				bag_name = str+"_RAW16.bag";
				rs2::pipeline pipe;
				rs2::config cfg;  
				cfg.enable_record_to_file(bag_name); // Start to save RAW 
				cfg.enable_stream(RS2_STREAM_COLOR,1920,1080, RS2_FORMAT_RAW16);
				if (auto_exposure == 1){
					rs2::pipeline_profile selection_0 = pipe.start(cfg); 
					rs2::device selected_device_0 = selection_0.get_device();
					auto color_sensor_0 = selected_device_0.first<rs2::color_sensor>();
					color_sensor_0.set_option(RS2_OPTION_ENABLE_AUTO_EXPOSURE, 0); 
					color_sensor_0.set_option(RS2_OPTION_EXPOSURE, exposure_time); // 33ms
					color_sensor_0.set_option(RS2_OPTION_GAIN, 64); 
					color_sensor_0.set_option(RS2_OPTION_SATURATION, 50); 
					color_sensor_0.set_option(RS2_OPTION_SHARPNESS, 50); 
					color_sensor_0.set_option(RS2_OPTION_ENABLE_AUTO_WHITE_BALANCE, 1);
					color_sensor_0.set_option(RS2_OPTION_BACKLIGHT_COMPENSATION, 0);
					color_sensor_0.set_option(RS2_OPTION_ENABLE_AUTO_EXPOSURE, 1); 
					color_sensor_0.set_option(RS2_OPTION_AUTO_EXPOSURE_PRIORITY, 1);
				}
				if (auto_exposure == 0){
					rs2::pipeline_profile selection = pipe.start(cfg);
					rs2::device selected_device = selection.get_device();
					auto color_sensor = selected_device.first<rs2::color_sensor>();
					color_sensor.set_option(RS2_OPTION_ENABLE_AUTO_EXPOSURE, 0);
					color_sensor.set_option(RS2_OPTION_EXPOSURE, exposure_time); // 33ms
					color_sensor.set_option(RS2_OPTION_GAIN, 128); // 128 gain
					color_sensor.set_option(RS2_OPTION_SATURATION, 50); 
					color_sensor.set_option(RS2_OPTION_SHARPNESS, 50); 
					color_sensor.set_option(RS2_OPTION_BACKLIGHT_COMPENSATION, 0);
					color_sensor.set_option(RS2_OPTION_ENABLE_AUTO_WHITE_BALANCE, 1);
				}
				sem_init(&InferSem, 0, 0);
				sem_init(&ToneSem, 0, 0);
				sem_init(&AWBSem, 0, 0);
				sem_init(&DisplaySem, 0, 0);
				pthread_t threads[5];
				int rc;
				//printf("Creating Inference thread\n");
				rc = pthread_create(&threads[0], NULL, Inference_thread, (void *) 0);
				if (rc) {
					cout << "Error:unable to create Inference thread," << rc << endl;
					exit(-1);
				}
				//printf("Creating Tone thread\n");
				rc = pthread_create(&threads[1], NULL, Tone_thread, (void *) 1);
				if (rc) {
					cout << "Error:unable to create Tone thread," << rc << endl;
					exit(-1);
				}
				//printf("Creating AWB thread\n");
				rc = pthread_create(&threads[2], NULL, AWB_thread, (void *) 2);
				if (rc) {
					cout << "Error:unable to create AWB thread," << rc << endl;
					exit(-1);
				}
				//printf("Creating Display thread\n");
				rc = pthread_create(&threads[3], NULL, Display_thread, (void *) 3);
				if (rc) {
					cout << "Error:unable to create Display thread," << rc << endl;
					exit(-1);
				}
				//printf("Creating Save thread\n");
				rc = pthread_create(&threads[4], NULL, Save_thread, (void *) 4);
				if (rc) {
					cout << "Error:unable to create Save thread," << rc << endl;
					exit(-1);
				}
				//for (auto i = 0; i < 30; ++i) pipe.wait_for_frames(); //  Capture 30 frames to give autoexposure, etc. a chance to settle 
				auto stop_0 = high_resolution_clock::now(); 
				while (pipe_run)
				{
					if (Capture_count == 0){
						stop_0 = high_resolution_clock::now();
					}
					rs2::frameset data = pipe.wait_for_frames(); // Wait for next set of frames from the camera
					data.keep();
					// Obtain RAW frame
					rs2::frame depth = data.get_color_frame();       // get color frame       
					Mat image(Size(1920, 1080), CV_16UC1, (void*)depth.get_data(),Mat::AUTO_STEP);    
					memcpy(img_input_buffer+capture_num*WIDTH*HEIGHT, image.ptr<ushort>(), WIDTH*HEIGHT*2);		
					CHECK(cudaMemcpy(input_buffer, img_input_buffer+capture_num*WIDTH*HEIGHT, WIDTH*HEIGHT*2, cudaMemcpyHostToDevice)); // Mat image to GPU
					// Input Pre-process
					imgNormalizeWrapper((ushort *)input_buffer,(float *)norm_input);	
					if (Capture_count == 0){  
						CHECK(cudaMemcpy(infer_buffer, (float *)norm_input, WIDTH*HEIGHT*4, cudaMemcpyDeviceToHost));		 
					} 
					if (Capture_count > 0){			
						if (buffer_num == infer_num){
							buffer_num = infer_num - 1;
							if (buffer_num == -1){
								buffer_num = buffer_size -1;				
							}
						}
						CHECK(cudaMemcpy(infer_buffer+buffer_num*WIDTH*HEIGHT, (float *)norm_input, WIDTH*HEIGHT*4, cudaMemcpyDeviceToHost));
					} 		
					auto stop_1 = high_resolution_clock::now();
					auto duration = duration_cast<microseconds>(stop_1 - stop_0);
					Capture_count += 1;
					buffer_num += 1;
					capture_num += 1;
					if (buffer_num == buffer_size){
						buffer_num = 0;				
					}
					if (capture_num == buffer_size){
						capture_num = 0;
					}
					float capture_time = (duration.count())/(1000000.0 * Capture_count);
					Capture_FPS = 1.0 / capture_time;
					sem_getvalue(&InferSem, &infer_value);
					if (infer_value < buffer_size){
						infer_value += 1;
						sem_post(&InferSem);
					}	
				}   
				// During the saving, if press q(quit), close saving pipeline, still stay in outer loop, the preview pipeline (before) will start
				// if press e(exit), close saving pipeline, exit_run = 1, !exit_run = 0, jump out while loop, close the program.
				// if press s(save), close current saving pipeline, start another preview pipeline
				//cout << "Capture Done." << endl;
				capture_run = 0;
				(void) pthread_join(threads[4], NULL);
				(void) pthread_join(threads[0], NULL);
				(void) pthread_join(threads[1], NULL);
				(void) pthread_join(threads[2], NULL);
				(void) pthread_join(threads[3], NULL);
				pipe.stop(); 
			}			
		}
		// Release stream and buffers
		cudaStreamDestroy(stream);
		CHECK(cudaFree(cuda_buffers[image_inputIndex]));
		CHECK(cudaFree(cuda_buffers[image_outputIndex]));
		CHECK(cudaFree(cuda_buffers[hidden_inputIndex]));
		CHECK(cudaFree(cuda_buffers[hidden_outputIndex]));
		CHECK(cudaFree(norm_input));
		CHECK(cudaFree(de_img));
		CHECK(cudaFree(input_buffer));
		CHECK(cudaFree(output_buffer));
		CHECK(cudaFree(output_buffer8T));
		CHECK(cudaFree(log_buffer));
		CHECK(cudaFree(base_buffer));
		CHECK(cudaFree(cuda_buffer_in));
		CHECK(cudaFree(cuda_buffer_out));
		CHECK(cudaFree(sk_0));
		CHECK(cudaFree(vk_0));
		CHECK(cudaFreeHost(output_image));
		CHECK(cudaFreeHost(bgra_out));
		CHECK(cudaFreeHost(img_bgr8T));
		CHECK(cudaFreeHost(log_img));
		CHECK(cudaFreeHost(cuda_in));
		CHECK(cudaFreeHost(cuda_out));
		CHECK(cudaFreeHost(Gaussian_img));
		CHECK(cudaFreeHost(img_input_buffer));
		CHECK(cudaFreeHost(infer_buffer));
		CHECK(cudaFreeHost(sk_buffer));
		CHECK(cudaFreeHost(hist_buffer));
		// Destroy the engine
		context->destroy();
		cuda_engine->destroy();
		runtime->destroy();  
	} 
	printf("Done\n");
	return EXIT_SUCCESS;
}
catch (const rs2::error & e)
{
    std::cerr << "RealSense error calling " << e.get_failed_function() << "(" << e.get_failed_args() << "):\n    " << e.what() << std::endl;
    return EXIT_FAILURE;
}
catch (const std::exception& e)
{
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
}
