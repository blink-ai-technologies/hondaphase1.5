#include <stdio.h>
#include "/usr/local/cuda-10.0/targets/aarch64-linux/include/cuda_fp16.h"
#include "/home/blink/realtimeV3/blink_config.h"
#include <cmath>

__global__ void dequantize_Bayer(float *input_img,float *output_img, float *de_output_img){
		int nidx = blockIdx.x*blockDim.x+threadIdx.x;
		int idx0 = nidx;
		int idx1 = nidx+kernel_height*kernel_width;
		int idx2 = nidx+kernel_height*kernel_width*2;
		int idx3 = nidx+kernel_height*kernel_width*3;
		
		int idx0_de = blockIdx.x*infer_width*2 + 2*threadIdx.x;
        int idx1_de = blockIdx.x*infer_width*2 + 2*threadIdx.x + 1;
        int idx2_de = idx0_de + infer_width;
        int idx3_de = idx1_de + infer_width;
        
        float x0 = 0.95*output_img[idx0]+0.05*input_img[idx0];
        float x0_2 = x0 * x0;
        de_output_img[idx0_de] = min((x0_2 + sqrt(x0_2*x0_2 + 16.0/max_pixel*x0_2)) / 2.0 + min_pixel, 1.0); // min ~ 1

        
        float x1 = 0.95*output_img[idx1]+0.05*input_img[idx1];
        float x1_2 = x1 * x1;
        de_output_img[idx1_de] = min((x1_2 + sqrt(x1_2*x1_2 + 16.0/max_pixel*x1_2)) / 2.0 + min_pixel, 1.0);
        
        
        float x2 = 0.95*output_img[idx2]+0.05*input_img[idx2];
        float x2_2 = x2 * x2;
        de_output_img[idx2_de] = min((x2_2 + sqrt(x2_2*x2_2 + 16.0/max_pixel*x2_2)) / 2.0 + min_pixel, 1.0);
        
        
        float x3 = 0.95*output_img[idx3]+0.05*input_img[idx3];
        float x3_2 = x3 * x3;
        de_output_img[idx3_de] = min((x3_2 + sqrt(x3_2*x3_2 + 16.0/max_pixel*x3_2)) / 2.0 + min_pixel, 1.0);
        
}

extern "C"  void dequantizeWrapper(float *input_img,float *output_img, float *de_output_img){
	dequantize_Bayer<<<kernel_height,kernel_width>>>(input_img,output_img,de_output_img);
}
