#include <stdio.h>
#include "/usr/local/cuda-10.0/targets/aarch64-linux/include/cuda_fp16.h"
#include "/home/blink/realtimeV3/blink_config.h"
#include <cmath>



__forceinline__ __device__ float full_img(float *output_img,int block_offset,int thread_offset,int total_offset){
        int real_blockId = blockIdx.x + block_offset;
        int real_threadId = threadIdx.x + thread_offset;
        if(real_blockId<0 || real_blockId>(kernel_height-1) || real_threadId<0 || real_threadId>(kernel_width-1)) return float(0.0);
        float out_value = output_img[real_blockId*kernel_width+real_threadId+total_offset*kernel_height*kernel_width];
		if(out_value>1.0) return float(1.0);
        return out_value;
}
__global__ void ImgNormalize(float *d_in, float *de_output_img){
        int idx0 = blockIdx.x*infer_width*2+2*threadIdx.x;
        int idx1 = blockIdx.x*infer_width*2+2*threadIdx.x+1;
        int idx2 = idx0+infer_width;
        int idx3 = idx1+infer_width;
        int nidx = blockIdx.x*blockDim.x+threadIdx.x;
        
        de_output_img[nidx] = d_in[idx0];   
        de_output_img[nidx+kernel_width*kernel_height]   = d_in[idx1];
        de_output_img[nidx+kernel_width*kernel_height*2] = d_in[idx2];
        de_output_img[nidx+kernel_width*kernel_height*3] = d_in[idx3];
}
__global__ void gray_log(float *de_output_img, float *img_log){
        
        // G R
        // B G
        int idx1 = blockIdx.x*infer_width*2 + 2*threadIdx.x;
        int idx2 = blockIdx.x*infer_width*2 + 2*threadIdx.x + 1;
        int idx3 = idx1 + infer_width;
        int idx4 = idx2 + infer_width;

		//from Gr
        float rr = (full_img(de_output_img,0,0,1) +full_img(de_output_img,0,-1,1))/2.0;
        float rg = full_img(de_output_img,0,0,0);
        float rb = (full_img(de_output_img,-1,0,2) + full_img(de_output_img,0,0,2))/2.0;
        img_log[idx1] = log(min(max(Pb*rb + Pg*rg + Pr*rr,min_pixel),1.0));
        
        // from R
        rr = full_img(de_output_img,0,0,1);
		rg = (full_img(de_output_img,0,0,0) + full_img(de_output_img,0,0,3) + full_img(de_output_img,-1,0,3) + full_img(de_output_img,0,1,0))/4.0;
        rb = (full_img(de_output_img,0,0,2) + full_img(de_output_img,-1,0,2) + full_img(de_output_img,-1,1,2) + full_img(de_output_img,0,1,2))/4.0; 
        img_log[idx2] = log(min(max(Pb*rb + Pg*rg + Pr*rr,min_pixel),1.0));
           
		//from B
        rr = (full_img(de_output_img,0,0,1)+full_img(de_output_img,0,-1,1)+full_img(de_output_img,1,0,1)+full_img(de_output_img,1,-1,1))/4.0;
        rg = (full_img(de_output_img,0,0,0)+full_img(de_output_img,0,0,3)+full_img(de_output_img,0,-1,3)+full_img(de_output_img,1,0,0))/4.0;
        rb = full_img(de_output_img,0,0,2);
        img_log[idx3] = log(min(max(Pb*rb + Pg*rg + Pr*rr,min_pixel),1.0));   
              
        //from Gb
        rr = (full_img(de_output_img,0,0,1) +full_img(de_output_img,1,0,1))/2.0;
        rg = full_img(de_output_img,0,0,3);
        rb = (full_img(de_output_img,0,0,2) + full_img(de_output_img,0,1,2))/2.0; 
        img_log[idx4] = log(min(max(Pb*rb + Pg*rg + Pr*rr,min_pixel),1.0));  
}

extern "C" void demosaicWrapper(float *d_in, float *de_output_img, float *img_log){
	// d_in: 1408x1056
	// img_log: log image of 1408x1056
	ImgNormalize<<<kernel_height,kernel_width>>>(d_in, de_output_img);
	gray_log<<<kernel_height,kernel_width>>>(de_output_img,img_log);
}
