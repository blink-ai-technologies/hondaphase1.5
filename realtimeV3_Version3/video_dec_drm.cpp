#include <unistd.h>
#include <drm/drm_fourcc.h>
#include "video_dec_drm.h"
#include <errno.h>
#include <fstream>
#include <iostream>
#include <linux/videodev2.h>
#include <malloc.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <drm_fourcc.h>
#include <linux/kd.h>
#include <linux/vt.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>


#include "NvUtils.h"
#include "video_dec_drm.h"
#include "tegra_drm.h"
#ifndef DOWNSTREAM_TEGRA_DRM
#include "tegra_drm_nvdc.h"
#endif
#include "NvApplicationProfiler.h"
#include "nvbuf_utils.h"
#define WIDTH 1920
#define HEIGHT 1080

char rgba[100][WIDTH*HEIGHT*3];
/*
void fill_buffer_color(void){
        for(int i=1;i<101;i++){
                char thisImage[WIDTH*HEIGHT*4];
                for(int m=0;m<WIDTH*HEIGHT*4;m+=4){
                        if(i % 3 == 0){ // B
                                thisImage[m + 0] = 255;  // B
                                thisImage[m + 1] = 0;    // G
                                thisImage[m + 2] = 0;    // R
                                thisImage[m + 3] = 255;  // A
                        } else if( i % 3 == 1){  // R for ARGB, B for ABGR
                                thisImage[m + 0] = 0;
                                thisImage[m + 1] = 0;
                                thisImage[m + 2] = 255;
                                thisImage[m + 3] = 255;
                        } else if( i % 3 == 2){ // G
                                thisImage[m + 0] = 0;
                                thisImage[m + 1] = 255;
                                thisImage[m + 2] = 0;
                                thisImage[m + 3] = 255;
                        }
                }
		// Color sequence : RGBRGBRGB
                memcpy(rgba[i-1], thisImage, WIDTH*HEIGHT*4);
        }
}
*/
void fill_buffer_color(void){
        for(int i=1;i<101;i++){
                char thisImage[WIDTH*HEIGHT*3];
                for(int m=0;m<WIDTH*HEIGHT*3;m+=3){
                        if(i % 3 == 0){ // B
                                thisImage[m + 0] = 255;  // B
                                thisImage[m + 1] = 255;    // G
                                thisImage[m + 2] = 255;    // R
                        } else if( i % 3 == 1){  // R for ARGB, B for ABGR
                                thisImage[m + 0] = 255;
                                thisImage[m + 1] = 255;
                                thisImage[m + 2] = 255;
                        } else if( i % 3 == 2){ // G
                                thisImage[m + 0] = 255;
                                thisImage[m + 1] = 255;
                                thisImage[m + 2] = 255;
                        }
                }
		// Color sequence : RGBRGBRGB
                memcpy(rgba[i-1], thisImage, WIDTH*HEIGHT*3);
        }
}


static void *
ui_render_loop_fcn(void *arg)
{
    context_t *ctx = (context_t *) arg;
    NvDrmFB ui_fb[3];
    unsigned int image_w = 1920; 
    unsigned int image_h = 1080;
    uint32_t frame = 0;
    

    ctx->drm_renderer->createDumbFB(image_w, image_h,DRM_FORMAT_ABGR8888,&ui_fb[0]);

    ctx->drm_renderer->createDumbFB(image_w, image_h,DRM_FORMAT_ABGR8888,&ui_fb[1]);
    
    ctx->drm_renderer->createDumbFB(image_w, image_h,DRM_FORMAT_ABGR8888,&ui_fb[2]);
    
    //ctx->drm_renderer->createDumbFB(image_w, image_h,DRM_FORMAT_RGB888,&ui_fb[0]);

    //ctx->drm_renderer->createDumbFB(image_w, image_h,DRM_FORMAT_RGB888,&ui_fb[1]);
    
    //ctx->drm_renderer->createDumbFB(image_w, image_h,DRM_FORMAT_RGB888,&ui_fb[2]);

    do {

	memcpy(ui_fb[(frame % 3) ].bo[0].data, rgba[frame %100], 1920*1080*3);
	printf("fb_id %d , rgba %d\n",ui_fb[frame %3].fb_id,frame %100);
        ctx->drm_renderer->setPlane(0, ui_fb[frame % 3].fb_id,
                100, 100, image_w, image_h,
                0, 0, image_w << 16, image_h << 16);
		usleep(2500000);
        frame++;

    } while (1);
}


static void
set_defaults(context_t * ctx)
{
    //ctx->dec = NULL;
    ctx->decoder_pixfmt = 1;
    ctx->crtc = 0;
    ctx->connector = 0;
    ctx->window_height = 1080;
    ctx->window_width = 1920;
    ctx->window_x = 0;
    ctx->window_y = 0;
    ctx->streamHDR = false;
    struct drm_tegra_hdr_metadata_smpte_2086 drm_metadata;
    memset(&drm_metadata,0,sizeof(struct drm_tegra_hdr_metadata_smpte_2086));
    ctx->drm_renderer = NvDrmRenderer::createDrmRenderer("renderer0",
            1920, 1080, 0, 0, ctx->connector, ctx->crtc, drm_metadata, ctx->streamHDR);
}



int
main(int argc, char *argv[])
{
    fill_buffer_color();
    context_t ctx;
    set_defaults(&ctx);
    ui_render_loop_fcn(&ctx);

    return 0;
}
