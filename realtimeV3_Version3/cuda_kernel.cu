#include <stdio.h>
#include "/usr/local/cuda-10.0/targets/aarch64-linux/include/cuda_fp16.h"
#include "/home/blink/realtimeV3/blink_config.h"

__global__ void imgNormalize(ushort *d_in, float *d_out){
	//float gain = 2;
        int idx1 = blockIdx.x*infer_width*2+2*threadIdx.x;
        int idx2 = blockIdx.x*infer_width*2+2*threadIdx.x+1;
        int idx3 = idx1 + infer_width;
        int idx4 = idx2 + infer_width;
        int nidx = blockIdx.x*blockDim.x+threadIdx.x;

        float x1 = float(d_in[idx1]>>6);
        float x11 = min(sqrt((x1+4)*max_pixel),max_pixel);
        d_out[nidx] = x1 / x11;
        

        float x2 = float(d_in[idx2]>>6);
        float x22 = min(sqrt((x2+4)*max_pixel),max_pixel);
        d_out[nidx+kernel_height*kernel_width] = x2 / x22;
        

        float x3 = float(d_in[idx3]>>6);
        float x33 = min(sqrt((x3+4)*max_pixel),max_pixel);
        d_out[nidx+kernel_height*kernel_width*2] = x3 / x33;
        

        float x4 = float(d_in[idx4]>>6);
        float x44 = min(sqrt((x4+4)*max_pixel),max_pixel);
        d_out[nidx+kernel_height*kernel_width*3] = x4 / x44;
}

extern "C" void imgNormalizeWrapper(ushort *d_in, float *d_out){
	imgNormalize<<<kernel_height,kernel_width>>>(d_in,d_out);
}

