#include <stdio.h>
#include "/usr/local/cuda-10.0/targets/aarch64-linux/include/cuda_fp16.h"
#include "/home/blink/realtimeV3/blink_config.h"
#include <cmath>



__forceinline__ __device__ float full_img(float *output_img,int block_offset,int thread_offset,int total_offset){
        int real_blockId = blockIdx.x + block_offset;
        int real_threadId = threadIdx.x + thread_offset;
        if(real_blockId<0 || real_blockId>(kernel_height-1) || real_threadId<0 || real_threadId>(kernel_width-1)) return float(0.0);
        float out_value = output_img[real_blockId*kernel_width+real_threadId+total_offset*kernel_height*kernel_width];
		if(out_value>1.0) return float(1.0);
        return out_value;
}
__global__ void dequantize(float *input_img,float *output_img, float *de_output_img){
		int nidx = blockIdx.x*blockDim.x+threadIdx.x;
		int idx0 = nidx;
		int idx1 = nidx+kernel_height*kernel_width;
		int idx2 = nidx+kernel_height*kernel_width*2;
		int idx3 = nidx+kernel_height*kernel_width*3;
		
        float x0 = DenoiseRatio*output_img[idx0]+(1-DenoiseRatio)*input_img[idx0];
		de_output_img[idx0] = x0 * x0;
        
        float x1 = DenoiseRatio*output_img[idx1]+(1-DenoiseRatio)*input_img[idx1];
        de_output_img[idx1] = x1 * x1;
        
        float x2 = DenoiseRatio*output_img[idx2]+(1-DenoiseRatio)*input_img[idx2];
        de_output_img[idx2] = x2 * x2;
        
        float x3 = DenoiseRatio*output_img[idx3]+(1-DenoiseRatio)*input_img[idx3];
        de_output_img[idx3] = x3 * x3;
}
__global__ void demosaic(float *de_output_img,float *dequantize_output_img,float *img_log){
        
        // G R
        // B G
        int idx1 = blockIdx.x*display_width*2 + 2*threadIdx.x;
        int idx2 = blockIdx.x*display_width*2 + 2*threadIdx.x + 1;
        int idx3 = idx1 + display_width;
        int idx4 = idx2 + display_width;

		//from Gr
        float rr = (full_img(de_output_img,0,0,1) +full_img(de_output_img,0,-1,1))/2.0;
        float rg = full_img(de_output_img,0,0,0);
        float rb = (full_img(de_output_img,-1,0,2) + full_img(de_output_img,0,0,2))/2.0;
        dequantize_output_img[idx1] = rg;
        img_log[idx1] = log(min(max(Pb*rb + Pg*rg + Pr*rr,min_pixel),1.0));
        
        // from R
        rr = full_img(de_output_img,0,0,1);
		rg = (full_img(de_output_img,0,0,0) + full_img(de_output_img,0,0,3) + full_img(de_output_img,-1,0,3) + full_img(de_output_img,0,1,0))/4.0;
        rb = (full_img(de_output_img,0,0,2) + full_img(de_output_img,-1,0,2) + full_img(de_output_img,-1,1,2) + full_img(de_output_img,0,1,2))/4.0; 
        dequantize_output_img[idx2] = rr;
        img_log[idx2] = log(min(max(Pb*rb + Pg*rg + Pr*rr,min_pixel),1.0)); 
             
		//from B
        rr = (full_img(de_output_img,0,0,1)+full_img(de_output_img,0,-1,1)+full_img(de_output_img,1,0,1)+full_img(de_output_img,1,-1,1))/4.0;
        rg = (full_img(de_output_img,0,0,0)+full_img(de_output_img,0,0,3)+full_img(de_output_img,0,-1,3)+full_img(de_output_img,1,0,0))/4.0;
        rb = full_img(de_output_img,0,0,2); 
        dequantize_output_img[idx3] = rb;     
        img_log[idx3] = log(min(max(Pb*rb + Pg*rg + Pr*rr,min_pixel),1.0)); 
        //from Gb
        rr = (full_img(de_output_img,0,0,1) +full_img(de_output_img,1,0,1))/2.0;
        rg = full_img(de_output_img,0,0,3);
        rb = (full_img(de_output_img,0,0,2) + full_img(de_output_img,0,1,2))/2.0;
        dequantize_output_img[idx4] = rg; 
        img_log[idx4] = log(min(max(Pb*rb + Pg*rg + Pr*rr,min_pixel),1.0)); 
}

extern "C"  void demosaicWrapper(float *input_img,float *output_img, float *de_output_img,float *dequantize_output_img, float *img_log){
	dequantize<<<kernel_height,kernel_width>>>(input_img,output_img,de_output_img);
	demosaic<<<kernel_height,kernel_width>>>(de_output_img,dequantize_output_img,img_log);
}
