#include <stdio.h>
#include "/usr/local/cuda-10.0/targets/aarch64-linux/include/cuda_fp16.h"
#include "/home/blink/realtimeV3/blink_config.h"
#include <cmath>
#include <math.h> 

    
__global__ void tone_curve(unsigned char *img_bayer8, float *img_bayer, float *img_log, float *img_base,float *sk,float *vk){ // Map base image according to tone curve
        int row = blockIdx.x;
		int col = threadIdx.x;
		int idx0 = row*display_width*2 + 2*col;
		int idx1 = row*display_width*2 + 2*col + 1;
		int idx2 = idx0 + display_width;
		int idx3 = idx1 + display_width;
		float sharp = sharp_level;
		// Bayer: G0 R0
		//        B0 G1
		// G0
		float base0 = img_base[idx0];
		float log0  = img_log[idx0];
		float relative_luma0 = base0 - MINV;
		int bin_index0 = int(relative_luma0/deltaHist);
		float tonedLogLuma0 = (relative_luma0 - bin_index0*deltaHist)*sk[bin_index0] + vk[bin_index0];
		float gain0 = exp((float)(tonedLogLuma0 + (sharp-1.0)*log0 - sharp*base0));
		gain0 = min(max(gain0,1.0),48.0);
		float G0 = img_bayer[idx0] ;
		G0 = min(G0 * gain0,1.0);
		img_bayer8[idx0] = (unsigned char)(G0*255);
		// R0
		float base1 = img_base[idx1];
		float log1  = img_log[idx1];
		float relative_luma1 = base1 - MINV;
		int bin_index1 = int(relative_luma1/deltaHist);
		float tonedLogLuma1 = (relative_luma1 - bin_index1*deltaHist)*sk[bin_index1] + vk[bin_index1];
		float gain1 = exp((float)(tonedLogLuma1 + (sharp-1.0)*log1 - sharp*base1));
		gain1 = min(max(gain1,1.0),48.0);
		float R0 = img_bayer[idx1] ;
		R0 = min(R0 * gain1,1.0);
		img_bayer8[idx1] = (unsigned char)(R0*255);
		// B0
		float base2 = img_base[idx2];
		float log2  = img_log[idx2];
		float relative_luma2 = base2 - MINV;
		int bin_index2 = int(relative_luma2/deltaHist);
		float tonedLogLuma2 = (relative_luma2 - bin_index2*deltaHist)*sk[bin_index2] + vk[bin_index2];
		float gain2 = exp((float)(tonedLogLuma2 + (sharp-1.0)*log2 - sharp*base2));
		gain2 = min(max(gain2,1.0),48.0);
		float B0 = img_bayer[idx2] ;
		B0 = min(B0 * gain2,1.0);
		img_bayer8[idx2] = (unsigned char)(B0*255);
		// G1
		float base3 = img_base[idx3];
		float log3  = img_log[idx3];
		float relative_luma3 = base3 - MINV;
		int bin_index3 = int(relative_luma3/deltaHist);
		float tonedLogLuma3 = (relative_luma3 - bin_index3*deltaHist)*sk[bin_index3] + vk[bin_index3];
		float gain3 = exp((float)(tonedLogLuma3 + (sharp-1.0)*log3 - sharp*base3));
		gain3 = min(max(gain3,1.0),48.0);
		float G1 = img_bayer[idx3] ;
		G1 = min(G1 * gain3,1.0);
		img_bayer8[idx3] = (unsigned char)(G1*255);
}

extern "C"  void toneWrapper(unsigned char *img_bayer8, float *img_bayer, float *img_log, float *img_base,float *sk0,float *vk0){
	tone_curve<<<kernel_height,kernel_width>>>(img_bayer8,img_bayer,img_log,img_base,sk0,vk0);
}
