#define Pr 0.299
#define Pg 0.587
#define Pb 0.114
#define NBIN 20
#define MINV -6.9304948
#define LogMax 5.54126
#define deltaHist 0.346525
#define N_iter 8
#define p0 0.00001
#define min_pixel 0.00097752
#define FROM_DISK true
#define exposure_time 333
#define full_width 1920
#define full_height 1080
#define bin_width 1280
#define bin_height 960
#define display_width 640
#define display_height 480
#define kernel_width 320
#define kernel_height 240
#define GAMMA_Correction 0.55
#define max_pixel 4095.0
#define sharp_level 3.0
#define Gaussian_size 5
#define Gaussian_sigma 0.7
#define DenoiseRatio 0.95



