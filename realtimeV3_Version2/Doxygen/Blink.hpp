/** 
 *  @file   Blink.hpp 
 *  @brief  Essential Processing Functions 
 *  @author Qingyao Jia 
 *  @date   2020-10-21 
 ***********************************************/
#include <opencv2/opencv.hpp>   // Include OpenCV API
typedef unsigned short ushort;

/// @brief Class for BlinkAI pipeline
class BlinkAI {
  public:
	/// @brief CUDA preprocessing function.
    /// @details The function takes RAW16 data as input and generate the pre-processed/normalized float image as output.
    /// @pre The input and output are on GPU. 
    ///
    /// @param input Input RAW16 data.
    /// @param output Output float pre-processed/normalized image. 
    /// 
	void PreProcess(ushort *input, float *output);
    
    /// @brief CUDA function to compute Log image from denoised RAW image.
    /// @details The function takes the dequantized & denoised RAW image as input. Generate log image of the denoised RAW image. 
    /// @pre The input and output are on GPU. 
    ///
    /// @param input Original RAW float image. 
    /// @param output Denoised RAW float image.
    /// @param de_output Intermediate dequantized output.
    /// @param dequantize_output Final dequantized output.   
    /// @param log_output Log image of the final dequantized and denoised output.   
    /// @param buffer A blank buffer on GPU to store the intermediate result.
    /// @param output Log image of the denoised RAW image.
    void GrayLog(float *input,float *output, float *de_output, float *dequantize_output,float *log_output);
    
    /// @brief CUDA function to apply tone curve.
    /// @details The function will take the tone curve and base image as input. Apply the tone curve on the input image and generate/output the tone-mapped result.
    /// @pre The input and output are on GPU. 
    ///
    /// @param toned_bayer Tone-mapped denoised RAW image.
    /// @param intput Denoised & dequantized RAW image. 
    /// @param img_log Log image of the denoised RAW image.
    /// @param img_base Base image of the denoised RAW image.
    /// @param sk Slope of the computed tone curve.
    /// @param vk Value of the computed tone curve.
    void ToneCurveApply(unsigned char *toned_bayer, float *intput, float *img_log, float *img_base,float *sk,float *vk);
    
    /// @brief Initialize histogram. 
    /// @details The function will initialize the histogram by flat/average.
    /// @pre The input and output are on CPU. 
    ///
    /// @param hist Histogram input/output. 
    void HistInitial(cv::Mat hist);
    
    /// @brief Compute tone curve slope. 
    /// @details The function will compute tone curve slope sk from histogram hist.
    /// @pre The input and output are on CPU. 
    ///
    /// @param hist Histogram of corresponding base image.
    /// @param sk Slope of the tone curve.
    void ComputeCurveSlope(cv::Mat hist, cv::Mat sk);
    
    /// @brief Compute tone curve value. 
    /// @details The function will compute tone curve value vk from slope sk.
    /// @pre The input and output are on CPU. 
    ///
    /// @param sk Slope of the tone curve.
    /// @param vk Value of the tone curve. 
    void ComputeCurveValue(cv::Mat sk, cv::Mat vk);
    
    /// @brief Gamma correction. 
    /// @details The function will apply Gamma correction on tone curve slope and value.
    /// @pre The input and output are on CPU. 
    ///
    /// @param sk Slope of the tone curve.
    /// @param vk Value of the tone curve. 
    void GammaCorrect(cv::Mat sk, cv::Mat vk);
    
	/// @brief Compute tone curve. 
    /// @details The function will take base image as input. It will compute and outpu the key parameters of the tone curve: Slope & Value.
    /// @pre The input and output are on CPU. 
    ///
    /// @param sk Slope of computed tone curve.
    /// @param vk Value of computed tone curve.
    /// @param num Order/Number of current input image.
    /// @param image Input base image.
    /// @param hist_buffer Histogram buffer which stores histogram of current input image and previous image.
    /// @param sk_buffer sk buffer which stores tone curve slope of current tone curve and previous tone curve.
    void ComputeToneCurve(cv::Mat sk,cv::Mat vk, int num, cv::Mat image, float *hist_buffer, float *sk_buffer);
    
    /// @brief Auto-White-Balance  
    /// @details The function will apply auto-white-balance on the input 8-bit BGR image.
    /// @pre The input and output are on CPU. 
    ///
    /// @param input Input 8-bit BGR image
    /// @param output Output 8-bit BGR image
    void AWB(cv::Mat output, cv::Mat input);
};
