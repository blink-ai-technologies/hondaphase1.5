#include <stdio.h>
#include "/usr/local/cuda-10.0/targets/aarch64-linux/include/cuda_fp16.h"
#include "/home/blink/realtimeV3/blink_config.h"
__global__ void PreProcessing(ushort *d_in, float *d_out){
	int idx1_0 = blockIdx.x*bin_width*4+4*threadIdx.x;
    int idx1_1 = blockIdx.x*bin_width*4+4*threadIdx.x+2;
    int idx1_2 = blockIdx.x*bin_width*4+4*threadIdx.x+bin_width*2;
    int idx1_3 = blockIdx.x*bin_width*4+4*threadIdx.x+bin_width*2+2;
    // R0
    int idx2_0 = blockIdx.x*bin_width*4+4*threadIdx.x+1;
    int idx2_1 = blockIdx.x*bin_width*4+4*threadIdx.x+3;
    int idx2_2 = blockIdx.x*bin_width*4+4*threadIdx.x+bin_width*2+1;
    int idx2_3 = blockIdx.x*bin_width*4+4*threadIdx.x+bin_width*2+3;
    
    // B0
    int idx3_0 = blockIdx.x*bin_width*4+4*threadIdx.x+bin_width;
    int idx3_1 = blockIdx.x*bin_width*4+4*threadIdx.x+bin_width+2;
    int idx3_2 = blockIdx.x*bin_width*4+4*threadIdx.x+bin_width*3;
    int idx3_3 = blockIdx.x*bin_width*4+4*threadIdx.x+bin_width*3+2;
        
    // G1
    int idx4_0 = blockIdx.x*bin_width*4+4*threadIdx.x+bin_width+1;
    int idx4_1 = blockIdx.x*bin_width*4+4*threadIdx.x+bin_width+3;
    int idx4_2 = blockIdx.x*bin_width*4+4*threadIdx.x+bin_width*3+1;
    int idx4_3 = blockIdx.x*bin_width*4+4*threadIdx.x+bin_width*3+3;
    
    int nidx = blockIdx.x*blockDim.x+threadIdx.x;
      
    float x1 = float(d_in[idx1_0]>>6) + float(d_in[idx1_1]>>6) + float(d_in[idx1_2]>>6) + float(d_in[idx1_3]>>6);
    d_out[nidx] = sqrt(x1 / max_pixel);
    
    float x2 = float(d_in[idx2_0]>>6) + float(d_in[idx2_1]>>6) + float(d_in[idx2_2]>>6) + float(d_in[idx2_3]>>6);
    d_out[nidx+kernel_height*kernel_width] = sqrt(x2 / max_pixel); 
      
    float x3 = float(d_in[idx3_0]>>6) + float(d_in[idx3_1]>>6) + float(d_in[idx3_2]>>6) + float(d_in[idx3_3]>>6);
    d_out[nidx+kernel_height*kernel_width*2] = sqrt(x3 / max_pixel); 
        
    float x4 = float(d_in[idx4_0]>>6) + float(d_in[idx4_1]>>6) + float(d_in[idx4_2]>>6) + float(d_in[idx4_3]>>6);
	d_out[nidx+kernel_height*kernel_width*3] = sqrt(x4 / max_pixel); 
}

extern "C"  void imgNormalizeWrapper(ushort *d_in,float *d_out){
	PreProcessing<<<kernel_height,kernel_width>>>(d_in,d_out);
}

